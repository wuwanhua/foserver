package com.fo.foserver.mapi.wx;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.alibaba.fastjson.JSONObject;
import com.fo.foserver.client.domain.user.UserDO;
import com.fo.foserver.client.service.user.UserService;
import com.fo.foserver.mapi.com.CommonConfig;
import com.fo.foserver.util.base.StringUtil;
import com.fo.foserver.util.http.HttpClientService;
import com.fo.foserver.util.result.Msg;
import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;

public class WxMapiImpl implements WxMapi{
	
	private static final Logger logger = LoggerFactory.getLogger(WxMapiImpl.class);
	@Resource
	private UserService userService;

	@Override
	public Result<JSONObject> wxAuth(String jsCode, Integer appId, String ip, String deciveId) {
		
		if(jsCode == null){
			return new Result<JSONObject>(new MsgException("参数不能为空！"));
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("appid", CommonConfig.APP_ID);
		params.put("secret", CommonConfig.APP_SECRET);
		params.put("js_code", jsCode);
		params.put("grant_type", CommonConfig.WX_JSCODE2SESSION_GRANT_TYPE);
		
		try{
			HttpClientService httpClientService = new HttpClientService();
			httpClientService.setRetry(CommonConfig.RETRY);
			httpClientService.setReadTimeout(CommonConfig.READ_TIMEOUT);
			httpClientService.init();
			String response = httpClientService.getHttpResponseContent(CommonConfig.WX_JS2SESSION, params, false, CommonConfig.CHARACTER_SET, CommonConfig.CHARACTER_SET);
			logger.warn("【微信 - 授权 - 响应】 res={}", response);
			JSONObject resJs = JSONObject.parseObject(response);
			if(resJs.containsKey("errcode") && resJs.get("errcode") != null){
				return new Result<JSONObject>(new MsgException(resJs.getString("errmsg")));
			}
			
			//更新用户数据
			String openid = resJs.getString("openid");
			String unionid = resJs.getString("unionid");
			String session_key = resJs.getString("session_key");
			UserDO userDO = null;
			Result<UserDO> userRes = userService.getByOpenId(openid);
			if(userRes.isSuccess() && userRes.getModel() != null){
				userDO = userRes.getModel();
				//更新用户登录信息
				userDO.setAppId(appId);
				userDO.setLastLoginIp(ip);
				userDO.setLastLoginDeviceId(deciveId);
				userDO.setLastLoginTime(new Date());
				Result<Boolean> updRes = userService.update(userDO);
				if(!StringUtil.isSuccess(updRes)){
					logger.warn("【更新用户登录信息 - 失败】 updRes={}, userDO={}", updRes, userDO);
				}
			}else{
				//注册
				userDO = new UserDO();
				userDO.setOpenId(openid);
				userDO.setUnionId(unionid);
				userDO.setLastLoginTime(new Date());
				Result<Long> uidRes = userService.create(userDO);
				if(!uidRes.isSuccess() || StringUtil.isEmptyLong(uidRes.getModel())){
					return new Result<JSONObject>(new MsgException("用户创建失败！"));
				}
				userDO.setUserId(uidRes.getModel());
			}
			
			JSONObject js = new JSONObject();
			js.put("loginId", userDO.getUserId());
			js.put("mobile", userDO.getMobile());
			
			//组装数据
			Result<JSONObject> res = new Result<JSONObject>();
			res.setModel(js);
			res.setMsgCode(Msg.LOGIN_SESSION.getMsgCode());
			return res;
		}catch(Throwable e){
			logger.warn("【微信 - 授权 - 异常】 e={}", e);
			return new Result<JSONObject>(new MsgException("授权失败！"));
		}
	}

}
