package com.fo.foserver.mapi.user;

import java.util.List;

import com.fo.foserver.client.domain.user.UserDO;
import com.fo.foserver.client.pojo.user.UserDTO;
import com.fo.foserver.client.pojo.user.UserQuery;
import com.fo.foserver.util.result.Result;

import net.sf.json.JSONObject;

/**
 * fo_user
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2017-12-16 21:27:16
 */
public interface UserMapi {
	
	/**
	 * 绑定手机号
	 * @param mobile
	 * @param code
	 * @param userId
	 * @return
	 */
	Result<Boolean> bindMobile(String mobile, String code, Long userId);
	Result<Boolean> bindMobileV2(String mobile, String code, Long userId, String pwd);
	
	/**
	 * 根据微信openid获取用户信息
	 * @return
	 */
	Result<JSONObject> getUserInfoByOpenId(String openId, Long loginId);
	
	/**
	 * 微信用户登录(用户不存在直接创建)
	 * @param userDTO
	 * @return
	 */
	Result<JSONObject> loginByWeixin(UserDTO userDTO);
	
	/**
	 * mapi登录
	 * @param mobile
	 * @param pwd
	 * @return
	 */
	Result<JSONObject> loginMapi(String mobile, String pwd);
	
	/**
	 * 注册
	 * @param mobile
	 * @param pwd
	 * @param vefiryCode
	 * @return
	 */
	Result<JSONObject> register(String mobile, String pwd, String vefiryCode);
	
    /**
     * 查找记录
     */
    Result<UserDO> get(Long id);

    /**
     * 分页查询
     */
    Result<List<UserDO>> search(UserQuery userQuery);

    /**
     * 记录总数
     */
    Result<Long> count(UserQuery userQuery);

    /**
     * 新增记录
     */
    Result<Long> create(UserDO userDO);

    /**
     * 修改记录
     */
    Result<Boolean> update(UserDO userDO);
    Result<Boolean> updateUserInfo(Long loginId, String content);

    /**
     * 删除记录
     */
    Result<Boolean> delete(Long id);
}