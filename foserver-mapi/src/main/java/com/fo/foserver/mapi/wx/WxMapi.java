package com.fo.foserver.mapi.wx;

import com.alibaba.fastjson.JSONObject;
import com.fo.foserver.util.result.Result;

public interface WxMapi {
	
	/**
	 * 微信小程序登录授权
	 * @param jsCode
	 * @return
	 */
	Result<JSONObject> wxAuth(String jsCode, Integer appId, String ip, String deciveId);

}
