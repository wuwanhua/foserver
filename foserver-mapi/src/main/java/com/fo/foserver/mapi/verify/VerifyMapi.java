package com.fo.foserver.mapi.verify;

import com.fo.foserver.util.result.Result;

public interface VerifyMapi {

	/**
	 * 发送短信验证码
	 * @param mobile  手机号
	 * @param sign	      业务标识   参考：VerifySignEnum
	 * @return
	 */
	Result<Boolean> sendVerifyCode(String mobile, String sign);

	/**
	 * 验证码检查
	 * @param mobile  手机号
	 * @param code	      手机验证码
	 * @param sign    业务标识   参考：VerifySignEnum
	 * @return
	 */
	Result<Boolean> checkVerifyCode(String mobile, String code, String sign);

	/**
	 * 验证码失效
	 * @param mobile   手机号  
	 * @param sign   业务标识   参考：VerifySignEnum
	 * @return
	 */
	Result<Boolean> removeVerifyCode(String mobile, String sign);
}
