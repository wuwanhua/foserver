package com.fo.foserver.mapi.practice;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.client.domain.practice.PracticeCategoryDO;
import com.fo.foserver.client.domain.practice.PracticeDO;
import com.fo.foserver.client.pojo.practice.PracticeCategoryQuery;
import com.fo.foserver.client.service.practice.PracticeCategoryService;
import com.fo.foserver.client.service.practice.PracticeService;
import com.fo.foserver.util.base.ArrayUtil;
import com.fo.foserver.util.base.StringUtil;
import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * fo_practice_category
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-01-07 10:04:42
 */
public class PracticeCategoryMtopImpl implements PracticeCategoryMtop {
    private static final Logger logger = LoggerFactory.getLogger(PracticeCategoryMtopImpl.class);;

    @Resource
    private PracticeCategoryService practiceCategoryService;
    @Resource
    private PracticeService practiceService;

    @Override
    public Result<PracticeCategoryDO> get(Long id) {
        
        if(id == null){
            return new Result<PracticeCategoryDO>(new MsgException("非法参数"));
        }
        
        return practiceCategoryService.get(id);
        
    }

    @Override
    public Result<JSONArray> search(PracticeCategoryQuery practiceCategoryQuery) {
        
        if(practiceCategoryQuery == null){
            return new Result<JSONArray>(new MsgException("非法参数"));
        }
        
        Result<List<PracticeCategoryDO>> listRes = practiceCategoryService.search(practiceCategoryQuery);
        if(!listRes.isSuccess() || ArrayUtil.isEmpty(listRes.getModel())){
        	return new Result<JSONArray>();
        }
        
        JSONArray arr = new JSONArray();
        JSONObject js = null;
        for(PracticeCategoryDO pra : listRes.getModel()){
        	js = new JSONObject();
        	js.put("themePic", pra.getCategoryImg());
        	js.put("themeName", pra.getCategoryName());
        	js.put("themeId", pra.getId());
        	arr.add(js);
        }
        
        return new Result<JSONArray>(arr);
    }

    @Override
    public Result<Long> count(PracticeCategoryQuery practiceCategoryQuery) {
        
        if(practiceCategoryQuery == null){
            return new Result<Long>(new MsgException("非法参数"));
        }
        
        return practiceCategoryService.count(practiceCategoryQuery);
        
    }

    @Override
    public Result<Long> create(PracticeCategoryDO practiceCategoryDO) {
        
        if(practiceCategoryDO == null){
            return new Result<Long>(new MsgException("非法参数"));
        }
        
        return practiceCategoryService.create(practiceCategoryDO);
        
    }

    @Override
    public Result<Boolean> update(PracticeCategoryDO practiceCategoryDO) {
        
        if(practiceCategoryDO == null){
            return new Result<Boolean>(new MsgException("非法参数"));
        }
        
        return  practiceCategoryService.update(practiceCategoryDO);
        
    }

    @Override
    public Result<Boolean> delete(Long id) {
        
        if(id == null){
            return new Result<Boolean>(new MsgException("非法参数"));
        }
        
        return practiceCategoryService.delete(id);
        
    }

	@Override
	public Result<JSONObject> fetchPracticeTags(Integer action, Long targetId) {

        
        if(action == null || targetId == null){
            return new Result<JSONObject>(new MsgException("非法参数"));
        }
        
        JSONObject js = new JSONObject();
        String tag = null;
        if(action == 1){
        	Result<PracticeCategoryDO> praRes = practiceCategoryService.get(targetId);
        	if(!praRes.isSuccess() || praRes.getModel() == null || StringUtil.isEmpty(praRes.getModel().getTag())){
        		return new Result<JSONObject>(js);
        	}
        	tag = praRes.getModel().getTag();
        }else if(action == 2){
        	Result<PracticeDO> praRes = practiceService.get(targetId);
        	if(!praRes.isSuccess() || praRes.getModel() == null || StringUtil.isEmpty(praRes.getModel().getTags())){
        		return new Result<JSONObject>(js);
        	}
        	tag = praRes.getModel().getTags();
        }else{
        	return new Result<JSONObject>(new MsgException("非法操作"));
        }
        
        
        String[] tagAr = tag.split(",");
        JSONArray arr = new JSONArray();
        JSONObject tem = null;
        for(int i = 0; i < tagAr.length; i ++){
        	tem = new JSONObject();
        	tem.put("name", tagAr[i]);
        	tem.put("id", i+1);
        	arr.add(tem);
        }
        js.put("tag", arr);
        return new Result<JSONObject>(js);
	}

}