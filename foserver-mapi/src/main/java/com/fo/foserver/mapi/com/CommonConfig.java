package com.fo.foserver.mapi.com;
/**
 * 公共配置
 */
public class CommonConfig {

	/**
	 * HTTP
	 */
	public static final String CHARACTER_SET = "UTF-8";
	public static final Integer READ_TIMEOUT = 2000;
	public static final Integer RETRY = 0;
	
	/**
	 * WX_JSCODE_2_SESSION
	 */
	public static String APP_ID = "wxc79e22d45dfe6d97";
	public static String APP_SECRET = "998737a82a5050a30845f71e7dc1fbd7";
//	public static String APP_ID = "wx6770d0afe2020b35";
//	public static String APP_SECRET = "10a34efb5bb67c968ea3c9f42c93f23e";
//	public static String APP_ID = "wx122daee62ea10597";
//	public static String APP_SECRET = "7a77acb601759b25d7a5f025680fa3da";
	public static String WX_JSCODE2SESSION_GRANT_TYPE = "authorization_code";
	public static final String WX_JS2SESSION = "https://api.weixin.qq.com/sns/jscode2session";//开发者服务器使用登录凭证 code 获取 session_key 和 openid
}
