package com.fo.foserver.mapi.verify;

import javax.annotation.Resource;

import com.fo.foserver.client.service.verify.VerifyService;
import com.fo.foserver.util.result.Result;

public class VerifyMapiImpl implements VerifyMapi {
	
	@Resource
	private VerifyService verifyService;

	@Override
	public Result<Boolean> sendVerifyCode(String mobile, String sign) {
		return verifyService.sendVerifyCode(mobile, sign);
	}

	@Override
	public Result<Boolean> checkVerifyCode(String mobile, String code, String sign) {
		return verifyService.checkVerifyCode(mobile, code, sign);
	}

	@Override
	public Result<Boolean> removeVerifyCode(String mobile, String sign) {
		return verifyService.removeVerifyCode(mobile, sign);
	}

}
