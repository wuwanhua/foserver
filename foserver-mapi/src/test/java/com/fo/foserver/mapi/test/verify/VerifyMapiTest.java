package com.fo.foserver.mapi.test.verify;

import javax.annotation.Resource;

import org.junit.Test;

import com.fo.foserver.mapi.test.BaseTest;
import com.fo.foserver.mapi.verify.VerifyMapi;
import com.fo.foserver.util.base.JsonUtil;
import com.fo.foserver.util.result.Result;

public class VerifyMapiTest extends BaseTest{
	
	@Resource
	private VerifyMapi verifyMapi;

	@Test
	public void sendVerifyCode() {
		Result<Boolean> result = verifyMapi.sendVerifyCode("13655812763", "login");
		System.out.println(JsonUtil.toJson(result));
	}
	
	@Test
	public void checkVerifyCode() {
		Result<Boolean> result = verifyMapi.checkVerifyCode("13655812763", "079140", "login");
		System.out.println(JsonUtil.toJson(result));
	}
	
	@Test
	public void removeVerifyCode() {
		Result<Boolean> result = verifyMapi.removeVerifyCode("13655812763", "login");
		System.out.println(JsonUtil.toJson(result));
	}

}
