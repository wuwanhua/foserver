package com.fo.foserver.mapi.test.user;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.fo.foserver.client.domain.user.UserDO;
import com.fo.foserver.client.pojo.user.UserDTO;
import com.fo.foserver.client.pojo.user.UserQuery;
import com.fo.foserver.mapi.test.BaseTest;
import com.fo.foserver.mapi.user.UserMapi;
import com.fo.foserver.mapi.wx.WxMapi;
import com.fo.foserver.util.base.JsonUtil;
import com.fo.foserver.util.result.Result;

import net.sf.json.JSONObject;

/**
 * fo_user
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2017-12-16 21:27:16
 */
public class UserMapiTest extends BaseTest{

	@Resource
    private UserMapi userMapi;
	@Resource
	private WxMapi wxMapi;
	
	@Test
    public void wxAuth() {
        Result<com.alibaba.fastjson.JSONObject> result = wxMapi.wxAuth("013BY2kD0X1iad2D3QnD0qe6kD0BY2k0", 1, "127.0.0.1", "test");
        System.out.println(JsonUtil.toJson(result));
        Assert.assertTrue(result.isSuccess());
    }
	
	@Test
    public void loginByWeixin() {
		UserDTO userDTO = new UserDTO();
		userDTO.setOpenId("wuwanhua-openid");
		userDTO.setNickname("wwuanhua");
		userDTO.setAppId(1);
		userDTO.setLastLoginDeviceId("ios");
		userDTO.setLastLoginIp("127.0.0.1");
        Result<JSONObject> result = userMapi.loginByWeixin(userDTO);
        System.out.println(JsonUtil.toJson(result));
        Assert.assertTrue(result.isSuccess());
    }
	
	@Test
    public void getUserInfoByOpenId() {
        Result<JSONObject> result = userMapi.getUserInfoByOpenId(null, 3012851089499091L);
        System.out.println(JsonUtil.toJson(result));
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void loginMapi() {
        Result<JSONObject> result = userMapi.loginMapi("13655812763", "666666");
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }
    
    @Test
    public void register() {
    	Result<JSONObject> result = userMapi.register("13003600100", "666666", "888888");
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void get() {
        Result<UserDO> result = userMapi.get(3L);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void search() {
        UserQuery userQuery = new UserQuery();
        Result<List<UserDO>> result = userMapi.search(userQuery);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void count() {
        UserQuery userQuery = new UserQuery();
        Result<Long> result = userMapi.count(userQuery);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void create() {
        UserDO userDO = new UserDO();
        Result<Long> result = userMapi.create(userDO);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void update() {
        UserDO userDO = new UserDO();
        Result<Boolean> result = userMapi.update(userDO);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }
    
    @Test
    public void updateUserInfo() {
       	JSONObject js = new JSONObject();
       	js.put("nickName", "wanhua");
       	js.put("avatarUrl", "http://www.baidu.com");
        Result<Boolean> result = userMapi.updateUserInfo(6012581610701205L, js.toString());
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void delete() {
        Result<Boolean> result = userMapi.delete(null);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void Configuration() {
        /*
        manager.xml
        <bean id="userManager" class="com.fo.foserver.core.manager.user.UserManagerImpl" />
        
        user.service.context.xml
        <bean id="userService" class="com.fo.foserver.service.user.UserServiceImpl" />
        
        foserver.service.xml
        <import resource="context/user.service.context.xml"/>
        
        user.mtop.context.xml
        <bean id="userMapi" class="com.fo.foserver.mtop.user.UserMtopImpl" />
        
        foserver.mtop.xml
        <import resource="context/user.mtop.context.xml"/>
        
        foserver.dubbo.provider.mtop.xml
        <dubbo:service interface="com.fo.foserver.client.mtop.user.UserMtop" ref="userMapi" />
        
        foserver.dubbo.provider.service.xml
        <dubbo:service interface="com.fo.foserver.client.service.user.UserService" ref="UserService" />
        */
    }
}