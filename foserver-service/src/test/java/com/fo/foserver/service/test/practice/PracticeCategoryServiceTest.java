package com.fo.foserver.service.test.practice;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.fo.foserver.client.domain.practice.PracticeCategoryDO;
import com.fo.foserver.client.pojo.practice.PracticeCategoryQuery;
import com.fo.foserver.client.service.practice.PracticeCategoryService;
import com.fo.foserver.service.test.BaseTest;
import com.fo.foserver.util.result.Result;

/**
 * fo_practice_category
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-01-07 10:04:42
 */
public class PracticeCategoryServiceTest extends BaseTest {

	@Resource
    private  PracticeCategoryService practiceCategoryService;


    @Test
    public void get() {
        Result<PracticeCategoryDO> result = practiceCategoryService.get(null);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void search() {
        PracticeCategoryQuery practiceCategoryQuery = new PracticeCategoryQuery();
        Result<List<PracticeCategoryDO>> result = practiceCategoryService.search(practiceCategoryQuery);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void count() {
        PracticeCategoryQuery practiceCategoryQuery = new PracticeCategoryQuery();
        Result<Long> result = practiceCategoryService.count(practiceCategoryQuery);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void create() {
        PracticeCategoryDO practiceCategoryDO = new PracticeCategoryDO();
        Result<Long> result = practiceCategoryService.create(practiceCategoryDO);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void update() {
        PracticeCategoryDO practiceCategoryDO = new PracticeCategoryDO();
        Result<Boolean> result = practiceCategoryService.update(practiceCategoryDO);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void delete() {
        Result<Boolean> result = practiceCategoryService.delete(null);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }
}