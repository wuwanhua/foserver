package com.fo.foserver.service.test.activity;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.fo.foserver.client.domain.activity.ActivityDO;
import com.fo.foserver.client.pojo.activity.ActivityQuery;
import com.fo.foserver.client.service.activity.ActivityService;
import com.fo.foserver.util.result.Result;

/**
 * fo_activity
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-02-06 15:02:02
 */
public class ActivityServiceTest {
    private static ApplicationContext applicationContext;

    private static ActivityService activityService;

    @BeforeClass
    public static void setUpClass() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("zjserver.service.test.xml");
        activityService =(ActivityService)applicationContext.getBean("activityService");
        Thread.sleep(500);
    }

    @Test
    public void get() {
        Result<ActivityDO> result = activityService.get(null);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void search() {
        ActivityQuery activityQuery = new ActivityQuery();
        Result<List<ActivityDO>> result = activityService.search(activityQuery);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void count() {
        ActivityQuery activityQuery = new ActivityQuery();
        Result<Long> result = activityService.count(activityQuery);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void create() {
        ActivityDO activityDO = new ActivityDO();
        Result<Long> result = activityService.create(activityDO);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void update() {
        ActivityDO activityDO = new ActivityDO();
        Result<Boolean> result = activityService.update(activityDO);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void delete() {
        Result<Boolean> result = activityService.delete(null);
        System.out.println(result);
        Assert.assertTrue(result.isSuccess());
    }
}