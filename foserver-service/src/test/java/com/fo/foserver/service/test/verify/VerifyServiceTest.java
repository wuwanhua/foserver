package com.fo.foserver.service.test.verify;

import javax.annotation.Resource;

import org.junit.Test;

import com.fo.foserver.client.service.verify.VerifyService;
import com.fo.foserver.service.test.BaseTest;
import com.fo.foserver.util.base.JsonUtil;
import com.fo.foserver.util.result.Result;

public class VerifyServiceTest extends BaseTest{
	
	@Resource
    private VerifyService verifyService;

	@Test
	public void sendVerifyCode() {
		Result<Boolean> result = verifyService.sendVerifyCode("13655812763", "login");
		System.out.println(JsonUtil.toJson(result));
	}
	
	@Test
	public void checkVerifyCode() {
		Result<Boolean> result = verifyService.checkVerifyCode("13655812763", "079140", "login");
		System.out.println(JsonUtil.toJson(result));
	}
	
	@Test
	public void removeVerifyCode() {
		Result<Boolean> result = verifyService.removeVerifyCode("13655812763", "login");
		System.out.println(JsonUtil.toJson(result));
	}

}
