package com.fo.foserver.service.practice;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.client.domain.practice.PracticeDailyDO;
import com.fo.foserver.client.pojo.practice.PracticeDailyQuery;
import com.fo.foserver.client.service.practice.PracticeDailyService;
import com.fo.foserver.core.manager.practice.PracticeDailyManager;
import com.fo.foserver.util.base.ArrayUtil;
import com.fo.foserver.util.base.KeyUtil;
import com.fo.foserver.util.img.ImgConfig;
import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;

/**
 * fo_practice_daily
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-01-07 10:05:20
 */
public class PracticeDailyServiceImpl implements PracticeDailyService {
    private static final Logger logger = LoggerFactory.getLogger(PracticeDailyServiceImpl.class);;

    @Resource
    private PracticeDailyManager practiceDailyManager;

    @Override
    public Result<PracticeDailyDO> get(Long id) {
        
        if(id == null){
            return new Result<PracticeDailyDO>(new MsgException("非法参数"));
        }
        
        PracticeDailyDO practiceDailyDO = practiceDailyManager.get(id);
        if(practiceDailyDO == null){
            return new Result<PracticeDailyDO>(new MsgException("未找到记录"));
        }
        
        practiceDailyDO.setDailyImg(ImgConfig.addCndImgUrl(practiceDailyDO.getDailyImg(), "", ""));
        return new Result<PracticeDailyDO>(practiceDailyDO);
        
    }

    @Override
    public Result<List<PracticeDailyDO>> search(PracticeDailyQuery practiceDailyQuery) {
        
        if(practiceDailyQuery == null){
            return new Result<List<PracticeDailyDO>>(new MsgException("非法参数"));
        }
        
        List<PracticeDailyDO> practiceDailyDO = practiceDailyManager.search(practiceDailyQuery);
        if(ArrayUtil.isEmpty(practiceDailyDO)){
            return new Result<List<PracticeDailyDO>>(new MsgException("未找到记录"));
        }
        
        for(PracticeDailyDO pra : practiceDailyDO){
        	pra.setDailyImg(ImgConfig.addCndImgUrl(pra.getDailyImg(), "", ""));
        }
        return new Result<List<PracticeDailyDO>>(practiceDailyDO);
        
    }

    @Override
    public Result<Long> count(PracticeDailyQuery practiceDailyQuery) {
        
        if(practiceDailyQuery == null){
            return new Result<Long>(new MsgException("非法参数"));
        }
        
        Long count = practiceDailyManager.count(practiceDailyQuery);
        if(count == null){
            return new Result<Long>(new MsgException("查询失败"));
        }
        
        return new Result<Long>(count);
        
    }

    @Override
    public Result<Long> create(PracticeDailyDO practiceDailyDO) {
        
        if(practiceDailyDO == null){
            return new Result<Long>(new MsgException("非法参数"));
        }
        
        practiceDailyDO.setDailyId(KeyUtil.getUniqId());
        Long id = practiceDailyManager.create(practiceDailyDO);
        if(id == null || id < 1L){
            return new Result<Long>(new MsgException("创建失败"));
        }
        
        return new Result<Long>(id);
        
    }

    @Override
    public Result<Boolean> update(PracticeDailyDO practiceDailyDO) {
        
        if(practiceDailyDO == null){
            return new Result<Boolean>(new MsgException("非法参数"));
        }
        
        Boolean result = practiceDailyManager.update(practiceDailyDO);
        if(result == null || !result){
            return new Result<Boolean>(new MsgException("修改失败"));
        }
        
        return new Result<Boolean>(result);
        
    }

    @Override
    public Result<Boolean> delete(Long id) {
        
        if(id == null){
            return new Result<Boolean>(new MsgException("非法参数"));
        }
        
        Boolean result = practiceDailyManager.delete(id);
        if(result == null || !result){
            return new Result<Boolean>(new MsgException("删除失败"));
        }
        
        return new Result<Boolean>(result);
        
    }

	@Override
	public Result<PracticeDailyDO> getDailyId(Long practiceDailyId) {
        
        if(practiceDailyId == null){
            return new Result<PracticeDailyDO>(new MsgException("非法参数"));
        }
        
        PracticeDailyDO practiceDailyDO = practiceDailyManager.getDailyId(practiceDailyId);
        if(practiceDailyDO == null){
            return new Result<PracticeDailyDO>(new MsgException("未找到记录"));
        }
        
        practiceDailyDO.setDailyImg(ImgConfig.addCndImgUrl(practiceDailyDO.getDailyImg(), "", ""));
        return new Result<PracticeDailyDO>(practiceDailyDO);
        
    }
}