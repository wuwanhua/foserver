package com.fo.foserver.service.activity;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.client.domain.activity.ActivityCodeDO;
import com.fo.foserver.client.pojo.activity.ActivityCodeQuery;
import com.fo.foserver.client.service.activity.ActivityCodeService;
import com.fo.foserver.core.manager.activity.ActivityCodeManager;
import com.fo.foserver.util.base.ArrayUtil;
import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;

/**
 * fo_activity_code
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-02-07 10:14:46
 */
public class ActivityCodeServiceImpl implements ActivityCodeService {
    private static final Logger logger = LoggerFactory.getLogger(ActivityCodeServiceImpl.class);;

    @Resource
    private ActivityCodeManager activityCodeManager;

    @Override
    public Result<ActivityCodeDO> get(Long id) {
        
        if(id == null){
            return new Result<ActivityCodeDO>(new MsgException("非法参数"));
        }
        
        ActivityCodeDO activityCodeDO = activityCodeManager.get(id);
        if(activityCodeDO == null){
            return new Result<ActivityCodeDO>(new MsgException("未找到记录"));
        }
        
        return new Result<ActivityCodeDO>(activityCodeDO);
        
    }

    @Override
    public Result<List<ActivityCodeDO>> search(ActivityCodeQuery activityCodeQuery) {
        
        if(activityCodeQuery == null){
            return new Result<List<ActivityCodeDO>>(new MsgException("非法参数"));
        }
        
        List<ActivityCodeDO> activityCodeDO = activityCodeManager.search(activityCodeQuery);
        if(ArrayUtil.isEmpty(activityCodeDO)){
            return new Result<List<ActivityCodeDO>>(new MsgException("未找到记录"));
        }
        
        return new Result<List<ActivityCodeDO>>(activityCodeDO);
        
    }

    @Override
    public Result<Long> count(ActivityCodeQuery activityCodeQuery) {
        
        if(activityCodeQuery == null){
            return new Result<Long>(new MsgException("非法参数"));
        }
        
        Long count = activityCodeManager.count(activityCodeQuery);
        if(count == null){
            return new Result<Long>(new MsgException("查询失败"));
        }
        
        return new Result<Long>(count);
        
    }

    @Override
    public Result<Long> create(ActivityCodeDO activityCodeDO) {
        
        if(activityCodeDO == null){
            return new Result<Long>(new MsgException("非法参数"));
        }
        
        Long id = activityCodeManager.create(activityCodeDO);
        if(id == null || id < 1L){
            return new Result<Long>(new MsgException("创建失败"));
        }
        
        return new Result<Long>(id);
        
    }

    @Override
    public Result<Boolean> update(ActivityCodeDO activityCodeDO) {
        
        if(activityCodeDO == null){
            return new Result<Boolean>(new MsgException("非法参数"));
        }
        
        Boolean result = activityCodeManager.update(activityCodeDO);
        if(result == null || !result){
            return new Result<Boolean>(new MsgException("修改失败"));
        }
        
        return new Result<Boolean>(result);
        
    }

    @Override
    public Result<Boolean> delete(Long id) {
        
        if(id == null){
            return new Result<Boolean>(new MsgException("非法参数"));
        }
        
        Boolean result = activityCodeManager.delete(id);
        if(result == null || !result){
            return new Result<Boolean>(new MsgException("删除失败"));
        }
        
        return new Result<Boolean>(result);
        
    }
}