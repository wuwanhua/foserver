package com.fo.foserver.service.verify;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.client.domain.user.UserDO;
import com.fo.foserver.client.enums.verify.EnvEnum;
import com.fo.foserver.client.enums.verify.RedisExpTimeEnum;
import com.fo.foserver.client.enums.verify.RedisPrefixEnum;
import com.fo.foserver.client.enums.verify.VerifySignEnum;
import com.fo.foserver.client.service.user.UserService;
import com.fo.foserver.client.service.verify.VerifyService;
import com.fo.foserver.util.base.CacheService;
import com.fo.foserver.util.base.StringUtil;
import com.fo.foserver.util.base.ValidateUtil;
import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;
import com.fo.foserver.util.sms.JavaSmsApi;

public class VerifyServiceImpl implements VerifyService {
	
	private static final Logger logger = LoggerFactory.getLogger(VerifyServiceImpl.class);
    private String env;
	
	@Resource
	private CacheService cacheService;
	@Resource
	private UserService userServcie;

	@Override
	public Result<Boolean> sendVerifyCode(String mobile, String sign) {
		
		//参数检查
		if(StringUtil.isEmpty(mobile) || !ValidateUtil.isMobile(mobile)) {
			return new Result<Boolean>(new MsgException("请输入合法的手机号码！"));
		}
		if(StringUtil.isEmpty(sign) || !VerifySignEnum.signMap.containsKey(sign)) {
			return new Result<Boolean>(new MsgException("请输入合法的业务标识！"));
		}
		
		//已注册用户无需发送注册验证码
		if(VerifySignEnum.REGISTER.getSign().equals(sign)){
			Result<UserDO> user = userServcie.getByMobile(mobile);
			if(user.isSuccess()){
				return new Result<Boolean>(new MsgException("该账号已注册, 请直接登录！"));
			}
		}

		//发送验证码
		String code = StringUtil.createRandom(true, 6);
		Map<String, String> paramVerifyCode = new HashMap<String, String>();
		paramVerifyCode.put("code", code);
		logger.info("发送验证码，手机：" + mobile + ", 验证码：" + code + ",sign=" + sign);
		boolean send = JavaSmsApi.sendSms(JavaSmsApi.VERIFY_TEMP, mobile, code);
		if(!send){
			return new Result<Boolean>(new MsgException("验证码发送失败, 请稍后重试！"));
		}

		//缓存验证码
		String key = StringUtil.addString("_", RedisPrefixEnum.VERIFY_CODE.getPrefix(), sign, mobile);
		boolean res = cacheService.set(key, code, RedisExpTimeEnum.VERIFY_CODE.getCode());
		if(!res){
			return new Result<Boolean>(new MsgException("验证码缓存失败, 请稍后重新发送！"));
		}
		return new Result<Boolean>(res);
	}

	@Override
	public Result<Boolean> checkVerifyCode(String mobile, String code, String sign) {
		
		//参数检查
		if(StringUtil.isEmpty(mobile) || !ValidateUtil.isMobile(mobile)) {
			return new Result<Boolean>(new MsgException("请输入合法的手机号码！"));
		}
		if(StringUtil.isEmpty(sign) || !VerifySignEnum.signMap.containsKey(sign)) {
			return new Result<Boolean>(new MsgException("请输入合法的业务标识！"));
		}
		if(StringUtil.isEmpty(code) || code.length() > 6) {
			return new Result<Boolean>(new MsgException("请输入合法的验证码！"));
		}
		
//		//如果内部测试环境，并且验证码为888888
		if(EnvEnum.DEV.getCode().equals(env) && "888888".equals(code)) {
			return new Result<Boolean>(true);
		}

		//获取缓存验证码
		String key = StringUtil.addString("_", RedisPrefixEnum.VERIFY_CODE.getPrefix(), sign, mobile);
		String cacheCode = (String)cacheService.get(key);
		if(StringUtil.isEmpty(cacheCode)) {
			return new Result<Boolean>(new MsgException("无效的验证码, 请重新发送！"));
		}
		
		//验证码检查
		if(!cacheCode.equals(code)){
			return new Result<Boolean>(new MsgException("请输入正确的验证码！"));
		}
		
		return new Result<Boolean>(true);
	}

	public void setEnv(String env) {
		this.env = env;
	}

	@Override
	public Result<Boolean> removeVerifyCode(String mobile, String sign) {
		
		//参数检查
		if(StringUtil.isEmpty(mobile) || !ValidateUtil.isMobile(mobile)) {
			return new Result<Boolean>(new MsgException("请输入合法的手机号码！"));
		}
		if(StringUtil.isEmpty(sign) || !VerifySignEnum.signMap.containsKey(sign)) {
			return new Result<Boolean>(new MsgException("请输入合法的业务标识！"));
		}
		
		//验证码使用完成后，失效
		String key = StringUtil.addString("_", RedisPrefixEnum.VERIFY_CODE.getPrefix(), sign, mobile);
		return new Result<Boolean>(cacheService.delete(key));
	}
	
}
