package com.fo.foserver.core.manager.user;

import com.fo.foserver.client.domain.user.UserDO;
import com.fo.foserver.client.pojo.user.UserQuery;
import com.fo.foserver.core.dao.user.UserDAO;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

/**
 * fo_user
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2017-12-16 21:27:16
 */
public class UserManagerImpl implements UserManager {
    @Resource
    private UserDAO userDAO;

    @Override
    public UserDO get(Long id) {
        
        if(id != null){
            return userDAO.get(id);
        }
        return null;
    }

    @Override
    public List<UserDO> search(UserQuery userQuery) {
        
        if(userQuery != null){
            return userDAO.search(userQuery);
        }
        return null;
    }

    @Override
    public Long count(UserQuery userQuery) {
        
        if(userQuery != null){
            return userDAO.count(userQuery);
        }
        return null;
    }

    @Override
    public Long create(UserDO userDO) {
        
        if(userDO == null){
            return null;
        }
        
        userDO.setCreatedTime(new Date());
        userDO.setUpdatedTime(new Date());
        
        if(userDAO.create(userDO)){
            return userDO.getUserId();
        }
        return 0L;
    }

    @Override
    public Boolean update(UserDO userDO) {
        
        if(userDO == null || userDO.getUserId() == null){
            return false;
        }
        
        userDO.setUpdatedTime(new Date());
        return userDAO.update(userDO);
    }

    @Override
    public Boolean delete(Long id) {
        
        if(id == null){
            return false;
        }
        return userDAO.delete(id);
    }

	@Override
	public UserDO getByMobile(String mobile) {
        
        if(mobile != null){
            return userDAO.getByMobile(mobile);
        }
        return null;
    }

	@Override
	public UserDO getByUserId(Long userId) {
        
        if(userId != null){
            return userDAO.getByUserId(userId);
        }
        return null;
    }

	@Override
	public UserDO getByOpenId(String openId) {
        
        if(openId != null){
            return userDAO.getByOpenId(openId);
        }
        return null;
    }
}