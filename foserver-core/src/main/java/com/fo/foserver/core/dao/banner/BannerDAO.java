package com.fo.foserver.core.dao.banner;

import java.util.List;

import com.fo.foserver.client.domain.banner.BannerDO;
import com.fo.foserver.client.pojo.banner.BannerQuery;

/**
 * fo_banner
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-01-02 22:18:57
 */
public interface BannerDAO {
    /**
     * 查找记录
     */
    BannerDO get(Long id);

    /**
     * 分页查询
     */
    List<BannerDO> search(BannerQuery model);

    /**
     * 记录总数
     */
    Long count(BannerQuery model);

    /**
     * 新增记录
     */
    boolean create(BannerDO model);

    /**
     * 修改记录
     */
    boolean update(BannerDO model);

    /**
     * 删除记录
     */
    boolean delete(Long id);
}