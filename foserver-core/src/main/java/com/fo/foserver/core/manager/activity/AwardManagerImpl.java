package com.fo.foserver.core.manager.activity;

import com.fo.foserver.client.domain.activity.AwardDO;
import com.fo.foserver.client.pojo.activity.AwardQuery;
import com.fo.foserver.core.dao.activity.AwardDAO;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

/**
 * fo_award
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-02-06 15:02:18
 */
public class AwardManagerImpl implements AwardManager {
    @Resource
    private AwardDAO awardDAO;

    @Override
    public AwardDO get(Long id) {
        
        if(id != null){
            return awardDAO.get(id);
        }
        return null;
    }

    @Override
    public List<AwardDO> search(AwardQuery awardQuery) {
        
        if(awardQuery != null){
            return awardDAO.search(awardQuery);
        }
        return null;
    }

    @Override
    public Long count(AwardQuery awardQuery) {
        
        if(awardQuery != null){
            return awardDAO.count(awardQuery);
        }
        return null;
    }

    @Override
    public Long create(AwardDO awardDO) {
        
        if(awardDO == null){
            return null;
        }
        
        awardDO.setCreatedTime(new Date());
        awardDO.setUpdatedTime(new Date());
        
        if(awardDAO.create(awardDO)){
            return awardDO.getId();
        }
        return 0L;
    }

    @Override
    public Boolean update(AwardDO awardDO) {
        
        if(awardDO == null || awardDO.getId() == null){
            return false;
        }
        
        awardDO.setUpdatedTime(new Date());
        return awardDAO.update(awardDO);
    }

    @Override
    public Boolean delete(Long id) {
        
        if(id == null){
            return false;
        }
        return awardDAO.delete(id);
    }
}