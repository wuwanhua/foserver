package com.fo.foserver.client.enums.verify;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 修行 -  周期 1-每日 2-每周 3-每月
 **/
public enum EnumPracticePeriod {
	
	DAY(1, "每日修行"),
	WEEK(2, "每周修行"),        		
	MONTH(3, "每月修行"),      
	;
	
	private Integer code;
	private String description;
	
	private EnumPracticePeriod(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public static final Map<Integer, EnumPracticePeriod> paperLogStatusMap = new HashMap<Integer, EnumPracticePeriod>();
    static {
    	for (EnumPracticePeriod gen: EnumSet.allOf(EnumPracticePeriod.class)){
    		paperLogStatusMap.put(gen.getCode(), gen);
    	}
    }
    
    public static String getDes(Integer code){
    	if(code == null || !paperLogStatusMap.containsKey(code)){
    		return "";
    	}
    	return paperLogStatusMap.get(code).getDescription();
    }
    
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
