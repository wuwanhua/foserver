package com.fo.foserver.client.enums.verify;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 手机验证码-业务标识
 */
public enum VerifySignEnum {
	
	LOGIN("login", "登陆"),
	FORGOT("forgot","找回密码"),
	REGISTER("register","注册"),
	THIRD_BIND("tbind","第三方授权绑定"),
	BIND("bind", "绑定"),
	SIGNIN("signin", "签到"),
	;
	
	private String  sign;
	private String description;
	
	private VerifySignEnum(String  sign, String description) {
		this.sign = sign;
		this.description = description;
	}
	
	public static final Map<String, VerifySignEnum> signMap = new HashMap<String, VerifySignEnum>();
    static {
    	for (VerifySignEnum gen: EnumSet.allOf(VerifySignEnum.class)){
    		signMap.put(gen.getSign(), gen);
    	}
    }

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
