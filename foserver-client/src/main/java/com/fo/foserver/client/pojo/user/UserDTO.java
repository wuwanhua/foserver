package com.fo.foserver.client.pojo.user;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class UserDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2118138192382636946L;

	/**
	 * 用户在微信公众号中的统一标识
	 */
	private String unionId;

	/**
	 * 微信openid
	 */
	private String openId;

	/**
	 * 用户昵称
	 */
	private String nickname;

	/**
	 * 用户性别 1-男 2-女
	 */
	private Integer gender;

	/**
	 * 用户头像
	 */
	private String avatar;

	/**
	 * 客户端来源(参考：EnumApp)
	 */
	private Integer appId;

	/**
	 * 用户最后登录IP地址
	 */
	private String lastLoginIp;

	/**
	 * 用户最后登录设备
	 */
	private String lastLoginDeviceId;

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getLastLoginDeviceId() {
		return lastLoginDeviceId;
	}

	public void setLastLoginDeviceId(String lastLoginDeviceId) {
		this.lastLoginDeviceId = lastLoginDeviceId;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

}
