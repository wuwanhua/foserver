package com.fo.foserver.client.enums.verify;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 项目当前环境 ：
 * 1， dev  内部测试环境
 * 2， pre  预发布环境
 * 3， prd  线上正式环境
 */
public enum EnvEnum {
	
	DEV("dev", "内部测试环境"),		
	PRE("pre", "预发布环境"),			    
	PRD("prd", "线上正式环境"),				
	;
	
	private String code;
	private String description;
	
	private EnvEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public static final Map<String, EnvEnum> envMap = new HashMap<String, EnvEnum>();
    static {
	    	for (EnvEnum gen: EnumSet.allOf(EnvEnum.class)){
	    		envMap.put(gen.getCode(), gen);
	    	}
    }
    
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
