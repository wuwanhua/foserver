package com.fo.foserver.client.enums.verify;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * redis存储key值的命名前缀
 */
public enum RedisPrefixEnum {
	
	VERIFY_CODE("vcode", "手机验证码"),
	GESTURE_PWD("gpwd", "手势密码"),
	REGION("region", "区域"),
	REGIONDO("regiondo", "区域"),
	ECONOMICS_PRAISE("epraise", "财经文章点赞"),
	;
	
	private String  prefix;
	private String description;
	
	private RedisPrefixEnum(String  prefix, String description) {
		this.prefix = prefix;
		this.description = description;
	}
	
	public static final Map<String, RedisPrefixEnum> prefixMap = new HashMap<String, RedisPrefixEnum>();
    static {
    	for (RedisPrefixEnum gen: EnumSet.allOf(RedisPrefixEnum.class)){
    		prefixMap.put(gen.getPrefix(), gen);
    	}
    }

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
