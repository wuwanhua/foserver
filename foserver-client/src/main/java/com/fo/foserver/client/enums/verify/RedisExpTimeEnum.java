package com.fo.foserver.client.enums.verify;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * redis 中数据存储有效时长
 * 单位：秒
 */
public enum RedisExpTimeEnum {
	
	UNLIMITED(-1, "无过期时间"),
	VERIFY_CODE(15 * 60, "验证码过期时间"),        		//15分钟
	GESTURE_PWD(24 * 60 * 60, "手势密码过期时间"),      //1天
	REGION(30 * 24 * 60 * 60, "区域信息过期时间"),      //30天
	;
	
	private Integer code;
	private String description;
	
	private RedisExpTimeEnum(Integer code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public static final Map<Integer, RedisExpTimeEnum> paperLogStatusMap = new HashMap<Integer, RedisExpTimeEnum>();
    static {
    	for (RedisExpTimeEnum gen: EnumSet.allOf(RedisExpTimeEnum.class)){
    		paperLogStatusMap.put(gen.getCode(), gen);
    	}
    }
    
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
