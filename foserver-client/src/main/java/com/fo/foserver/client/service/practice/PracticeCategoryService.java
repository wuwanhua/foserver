package com.fo.foserver.client.service.practice;

import java.util.List;

import com.fo.foserver.client.domain.practice.PracticeCategoryDO;
import com.fo.foserver.client.pojo.practice.PracticeCategoryQuery;
import com.fo.foserver.util.result.Result;

/**
 * fo_practice_category
 * This class was generated by MyBatis Generator.
 * mybatis-generator@2018-01-07 10:04:42
 */
public interface PracticeCategoryService {
    /**
     * 查找记录
     */
    Result<PracticeCategoryDO> get(Long id);

    /**
     * 分页查询
     */
    Result<List<PracticeCategoryDO>> search(PracticeCategoryQuery practiceCategoryQuery);

    /**
     * 记录总数
     */
    Result<Long> count(PracticeCategoryQuery practiceCategoryQuery);

    /**
     * 新增记录
     */
    Result<Long> create(PracticeCategoryDO practiceCategoryDO);

    /**
     * 修改记录
     */
    Result<Boolean> update(PracticeCategoryDO practiceCategoryDO);

    /**
     * 删除记录
     */
    Result<Boolean> delete(Long id);
}