package com.fo.foserver.client.pojo.attention;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * fo_attention This class was generated by MyBatis Generator.
 * mybatis-generator@2018-01-16 20:33:19
 */
public class AttentionQuery implements Serializable {
	/**
	 * 关注表 - 主键ID
	 */
	private Long id;

	/**
	 * 关注表 - 用户ID
	 */
	private Long userId;

	/**
	 * 目标ID
	 */
	private Long targetId;

	/**
	 * 关注类型 1-修行
	 */
	private Integer type;

	/**
	 * 关注表 - 创建时间
	 */
	private Date createdTime;

	/**
	 * 关注表 - 修改时间
	 */
	private Date updatedTime;

	/**
	 * 分页-offset
	 */
	private Integer offset;

	/**
	 * 分页-limit
	 */
	private Integer limit;
	private Integer page;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}