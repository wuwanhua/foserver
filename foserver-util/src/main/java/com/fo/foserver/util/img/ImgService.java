package com.fo.foserver.util.img;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;

/**
 * 图片上传
 */
public class ImgService {
	
	public static Result<Boolean> uploadQiniu(String filename, InputStream content){
		
		try{
			StringMap params = new StringMap().put("x:foo", "foo_val");
			Auth auth = Auth.create(ImgConfig.APP_ID, ImgConfig.APP_SECRET);
		    String token = auth.uploadToken(ImgConfig.BUCKET_H5);
			Zone zone = Zone.zone0();
			UploadManager uploadManager = new UploadManager(new Configuration(zone));
			Response res = uploadManager.put(content, filename, token, params, null);
			StringMap m = res.jsonToMap();
			if(!m.get("x:foo").equals("foo_val")){
				return new Result<Boolean>(new MsgException("上传文件失败！"));
			}
		}catch(Throwable e){
			return new Result<Boolean>(new MsgException("上传文件异常！"));
		}
		return new Result<Boolean>(true);
	}
	
	public static void main(String[] args) {
		try{
			String filename = "java/test.html";
			File f = TempFile.createFile(1024);
			uploadQiniu(filename, new FileInputStream(f));
		}catch(Throwable e){
			System.out.println(e);
		}
	}

}
