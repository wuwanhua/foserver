package com.fo.foserver.util.base;

import com.fo.foserver.util.result.Result;

/**
 * Result公共类
 */
public class ResultUtil {
	
	public static Boolean isSuccess(Result<?> result){
		
		if(result == null || !result.isSuccess() || result.getModel() == null){
			return false;
		}
		
		return true;
	}
	
	public static Boolean isSuccessLong(Result<?> result){
		
		if(result == null || !result.isSuccess() || result.getModel() == null || (Long)result.getModel() < 1L){
			return false;
		}
		
		return true;
	}
	
	public static Boolean isSuccessBoolean(Result<?> result){
		
		if(result == null || !result.isSuccess() || result.getModel() == null || !(Boolean)result.getModel()){
			return false;
		}
		
		return true;
	}
	

}
