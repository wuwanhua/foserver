package com.fo.foserver.util.base;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JsonUtil {

	/**
	 * json
	 */
	private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

	/**
	 * jackson mapper
	 */
	private static final ObjectMapper mapper = newObjectMapper();

	private static ObjectMapper newObjectMapper() {
		return new ObjectMapper();
	}

	/**
	 * object to json
	 * 
	 * @param object
	 * @param jsonConvertType
	 * @return
	 */
	public static String toJson(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (Exception e) {
			logger.error("JsonUtil.toJson error : ", e);
		}
		return null;
	}

	/**
	 * json to object
	 * 
	 * @param json
	 * @param classz
	 * @param jsonConvertType
	 * @param <T>
	 * @return
	 */
	public static <T> T toObject(String json, Class<T> classz) {
		try {
			return mapper.readValue(json, classz);
		} catch (Exception e) {
			logger.error("JsonUtil.toObject error : " + json, e);
		}

		return null;
	}

	/**
	 * json to object
	 
	 * 该方法可用于识别存在泛型的class
	 * @param json
	 * @param typeReference 使用方法 new TypeReference<ArrayList<String>>(){}
	 * @param <T>
	 * @return
	 */
	public static <T> T toObject(String json, TypeReference<T> typeReference) {
		try {
			return mapper.readValue(json, typeReference);
		} catch (Exception e) {
			logger.error("JsonUtil.toObject error : " + json, e);
		}
		return null;
	}

	/**
	 * 将对象转换为另一个类的对象 这个方法不是public的，因为把这个方法的出口放在BeanCopierUtil里面
	 * 请使用 BeanCopierUtil.convertValue
	 
	 * @param source
	 * @param clazz
	 * @return
	 */
	static <T> T convertValue(Object source, Class<T> clazz) {
		return mapper.convertValue(source, clazz);
	}

	/**
	 * 将对象转换为另一个类的对象 这个方法不是public的，因为把这个方法的出口放在BeanCopierUtil里面
	 * 请使用 BeanCopierUtil.convertValue
	 
	 * @param <T>
	 * @param source
	 * @param typeReference
	 * @return
	 */
	static <T> T convertValue(Object source, TypeReference<T> typeReference) {
		return mapper.convertValue(source, typeReference);
	}

	/**
	 * 提供一些修改时间默认格式的jackson处理工具
	 */
	private static Map<String, ObjectMapper> dateFormatMappers = new HashMap<String, ObjectMapper>(3);

	/**
	 * 在ObjectMapper中使用SimpleDateFormat是线程安全的 , 见StdDeserializationConfig.getDateFormat(), 内部是clone的
	 * @param dateFormat
	 * @return
	 */
	private static ObjectMapper newObjectMapper(String dateFormat) {
		ObjectMapper mapper = newObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat(dateFormat));
		return mapper;
	}

	/**
	 * 兼容以前的客户端，把日期类型默认转换成long类型
	 */
	private static ObjectMapper longDateObjectMapper = newObjectMapper();

	/**
	 * object to json
	 * 日期默认转换成long型时间
	 * @param object
	 * @return
	 */
	public static String toJson(Object object, String dateFormat) {
		ObjectMapper mapper = dateFormatMappers.get(dateFormat);
		if (mapper == null) {
			mapper = newObjectMapper(dateFormat);
		}
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			logger.error("JsonUtil.toJson error, dateFormat : " + dateFormat, e);
		}
		return null;
	}

	/**
	 * json to object
	 
	 * 该方法可用于识别存在泛型的class
	 * @param json
	 * @param typeReference 使用方法 new TypeReference<ArrayList<String>>(){}
	 * @param dateFormat 对日期类型进行指定格式的format
	 * @param <T>
	 * @return
	 */
	public static <T> T toObject(String json, TypeReference<T> typeReference, String dateFormat) {
		ObjectMapper mapper = dateFormatMappers.get(dateFormat);
		if (mapper == null) {
			mapper = newObjectMapper(dateFormat);
		}
		try {
			return mapper.readValue(json, typeReference);
		} catch (Exception e) {
			logger.error("JsonUtil.toObject json : " + json, e);
		}
		return null;
	}

	/**
	 * object to json
	 
	 * 日期默认转换成long型时间
	 * @param object
	 * @return
	 */
	public static String toJsonDateAsLong(Object object) {
		try {
			return longDateObjectMapper.writeValueAsString(object);
		} catch (Exception e) {
			logger.error("JsonUtil.toJsonDateAsLong error : ", e);
		}
		return null;
	}

	/**
	 * 由list转换的json返回list
	 * 
	 * @param json
	 *            {@code json}数据
	 * @return List
	 */
	public static List<String> getList(String json) {
		List<String> list = new ArrayList<String>();
		Map<String, String> map = getMap(json);
		for (Integer i = 0; i < map.size(); i++) {
			list.add(map.get(i.toString()));
		}
		return list;
	}

	/**
	 * 简单一维非对象或集合,字符串json转换map
	 * 
	 * @param json
	 *            {@code json}数据
	 * @return Map
	 */
	public static Map<String, String> getMap(String json) {
		Map<String, String> map = new HashMap<String, String>();
		json = HtmlUtil.delSpaceAndEnterTag(json);
		json = json.replace("[{", "");
		json = json.replace("}]", "");
		json = json.replace("\"", "");
		String str[] = json.split(",");
		for (int i = 0; i < str.length; i++) {
			map.put(str[i].substring(0, str[i].indexOf(":")), str[i].substring(str[i].indexOf(":") + 1, str[i].length()));
		}
		return map;
	}

	/**
	 * String类型的list转成json格式
	 * 
	 * @param list
	 *            {@code list}
	 * @return Json
	 */
	public static String getJson(List<String> list) {
		String begin = "[{";
		String end = "}]";
		String tag = ",";
		String middle = "";
		for (int i = 0; i < list.size(); i++) {
			middle += i == list.size() - 1 ? "\"" + i + "\":\"" + list.get(i) + "\""
					: "\"" + i + "\":\"" + list.get(i) + "\"" + tag;
		}
		return begin + middle + end;
	}

	/**
	 * String类型的map转成json格式
	 * 
	 * @param map
	 *            {@code map}
	 * @return Json
	 */
	public static String getJson(Map<String, String> map) {
		String begin = "[{";
		String end = "}]";
		String middle = "";
		Set<String> keys = map.keySet();
		int j = 1;
		for (String key : keys) {
			middle += j == keys.size() ? "\"" + key + "\":\"" + map.get(key) + "\"" : "\"" + key + "\":\"" + map.get(key) + "\",";
			j++;
		}
		return begin + middle + end;
	}

	/**
	 * 从 JSONObject 获取 字符串的key
	 * @param object
	 * @return
	 */
	public static String getStrFromJsonObject(JSONObject jsonObject, String key) {

		// 如果 object == null，则 返回 ""
		if (jsonObject == null || StringUtils.isEmpty(key)) {
			return "";
		}

		return jsonObject.has(key) ? jsonObject.getString(key) : "";
	}

	/**
	 * 从 JSONObject 获取 object
	 * @param object
	 * @return
	 */
	public static Object getObjFromJsonObject(JSONObject jsonObject, String key) {

		// 如果 object == null，则 返回 null
		if (jsonObject == null || StringUtils.isEmpty(key)) {
			return null;
		}

		return jsonObject.has(key) ? jsonObject.get(key) : null;
	}

	/**
	 * 从 JSONObject 获取 JSONObject
	 * @param object
	 * @return
	 */
	public static JSONObject getJSONObjectFromJsonObject(JSONObject jsonObject, String key) {

		// 如果 object == null，则 返回 null
		if (jsonObject == null || StringUtils.isEmpty(key)) {
			return null;
		}

		return jsonObject.has(key) ? jsonObject.getJSONObject(key) : null;
	}

	/**
	 * 从 JSONObject 获取 Integer
	 * @param object
	 * @return
	 */
	public static Integer getIntegerFromJsonObject(JSONObject jsonObject, String key) {

		// 如果 object == null，则 返回 null
		if (jsonObject == null || StringUtils.isEmpty(key)) {
			return null;
		}

		return jsonObject.has(key) ? jsonObject.getInt(key) : null;
	}

	/**
	 * 从 JSONObject 获取 Long
	 * @param object
	 * @return
	 */
	public static Long getLongFromJsonObject(JSONObject jsonObject, String key) {

		// 如果 object == null，则 返回 null
		if (jsonObject == null || StringUtils.isEmpty(key)) {
			return null;
		}

		return jsonObject.has(key) ? jsonObject.getLong(key) : null;
	}

	/**
	 * 从 JSONObject 获取 Long
	 * @param object
	 * @return
	 */
	public static JSONArray getJSONArrayFromJsonObject(JSONObject jsonObject, String key) {

		// 如果 object == null，则 返回 null
		if (jsonObject == null || StringUtils.isEmpty(key)) {
			return null;
		}

		return jsonObject.has(key) ? jsonObject.getJSONArray(key) : null;
	}
}
