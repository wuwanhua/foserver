package com.fo.foserver.util.serialization;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author wuqi
 */
public class JsonSerialization implements Serialization<byte[]> {

    private static final String NAMESPACE = "teslaSpace";

    static {
        if (!JSON.DEFAULT_TYPE_KEY.equals(NAMESPACE)) {
            JSON.DEFAULT_TYPE_KEY = NAMESPACE;
        }
    }

    private SerializerFeature[] serializerFeatures;

    private Feature[] features = new Feature[0];

    public JsonSerialization() {
        this(new SerializerFeature[0]);
    }

    public JsonSerialization(SerializerFeature... serializerFeatures) {
        this.serializerFeatures = serializerFeatures;
    }

    public JsonSerialization(SerializerFeature[] serializerFeatures, Feature[] deserializeFeatures) {
        this.serializerFeatures = serializerFeatures;
        this.features = deserializeFeatures;
    }

    @Override
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object, this.serializerFeatures);

    }

    @Override
    public Object deserialize(byte[] serializedValue) {
        return JSON.parse(serializedValue, this.features);
    }

}
