package com.fo.foserver.util.base;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
/**
 * 数据类型处理
 */
@SuppressWarnings("unchecked") 
public class DataTypeUtil {
	
	/**
	 * 将参数转换为指定类型
	 * 类型支持: String, Integer, Long, Double, BigDecimal
	 * 参数为空或非支持类型,返回null
	 * @param object
	 * @param clazz
	 * @return
	 */
	public static <T> T changeType(Object object, Class<T> clazz){
		
		if(ObjectUtil.isEmpty(object)){
			return null;
		}
		
		if(clazz.getName().equals(String.class.getName())){
			return (T)object.toString();
		}
		if(clazz.getName().equals(Long.class.getName())){
			return (T)(Long)(Long.parseLong(object.toString()));		
		}
		if(clazz.getName().equals(Integer.class.getName())){
			return (T)(Integer)Integer.parseInt(object.toString());
		}
		if(clazz.getName().equals(Double.class.getName())){
			return (T)(Double)Double.parseDouble(object.toString());
		}
		if(clazz.getName().equals(BigDecimal.class.getName())){
			return (T) new BigDecimal(object.toString());
		}
		
		return null;
	}
	
	/**
	 * @Title: initData
	 * @Description: 当数据为空的时候初始化数据       String:""; Long:0L; Integer:0; Double:0; BigDecimal:0
	 * @param @param object
	 * @param @param clazz
	 * @param @return   
	 * @return T    
	 * @throws
	 */
	public static <T> T initData(Object object, Class<T> clazz){
		
		boolean isNull = false;
		if(ObjectUtil.isEmpty(object)){
			isNull = true;
		}
		
		String nullString = StringUtils.EMPTY;
		Long nullLong = 0L;
		Double nullDouble = 0.0D;
		Integer nullInteger = 0;
		BigDecimal nullBigDecimal = new BigDecimal(0);
		
		if(clazz.getName().equals(String.class.getName())){
			return isNull?(T) nullString:(T)object.toString();
		}
		if(clazz.getName().equals(Long.class.getName())){
			return isNull?(T) nullLong:(T)(Long)(Long.parseLong(object.toString()));		
		}
		if(clazz.getName().equals(Integer.class.getName())){
			return isNull?(T) nullInteger:(T)(Integer)Integer.parseInt(object.toString());
		}
		if(clazz.getName().equals(Double.class.getName())){
			return isNull?(T)nullDouble:(T)(Double)Double.parseDouble(object.toString());
		}
		if(clazz.getName().equals(BigDecimal.class.getName())){
			return isNull?(T)nullBigDecimal:(T) new BigDecimal(object.toString());
		}
		
		return null;
	}

}
