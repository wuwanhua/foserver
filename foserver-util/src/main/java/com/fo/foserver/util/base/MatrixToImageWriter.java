package com.fo.foserver.util.base;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
/**
 * 用于二维码生成
 */
public class MatrixToImageWriter {
	
	
	   public static final int BLACK = 0xFF000000;  
	   public static final int WHITE = 0xFFFFFFFF;  
	   public static final int BLUE = 0xFF0075A4;
	    
	   public static BufferedImage toBufferedImage(BitMatrix matrix) {  
	     int width = matrix.getWidth();  
	     int height = matrix.getHeight();  
	     BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);  
	     for (int x = 0; x < width; x++) {  
	       for (int y = 0; y < height; y++) {  
	         image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);  
	       }  
	     }  
	     return image;  
	   }  
	   
	   /**
	    * @Title: toBufferedImageV2
	    * @Description: 去除了白边
	    * @param @param matrix
	    * @param @param rgb
	    * @param @return   
	    * @return BufferedImage    
	    * @throws
	    */
	   public static BufferedImage toBufferedImageV2(BitMatrix matrix, int rgb) {  
		   
		     int width = matrix.getWidth();  
		     int height = matrix.getHeight();  
		     //获取锚点
		     Integer a = null;
		     Integer b = null;
		     
		     for (int x = 0; x < width; x++) {  
			     for (int y = 0; y < height; y++) {  
				     if(matrix.get(x, y) && a == null && b == null){
				    		a = x;
				    		b = y;
				     }
			     }  
			 } 
		     
		     a = a -1;
		     b = b -1;
		     width = width-(2*a);
		     height = height-(2*b);
		     BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);  
		     
		     for (int x = 0; x < width; x++) {  
		       for (int y = 0; y < height; y++) {  
		         image.setRGB(x, y, matrix.get(x+a, y+b) ? rgb : WHITE);  
		       }  
		     }  
		     return image;  
	   } 
	    
	   public static void writeToFile(BitMatrix matrix, String format, File file)  
	       throws IOException {  
	     BufferedImage image = toBufferedImage(matrix);  
	     if (!ImageIO.write(image, format, file)) {  
	       throw new IOException("Could not write an image of format " + format + " to " + file);  
	     }  
	   }  
	   
	   public static void writeToFileV2(BitMatrix matrix, String format, File file, int rgb)  
		       throws IOException {  
		     BufferedImage image = toBufferedImageV2(matrix, rgb);  
		     if (!ImageIO.write(image, format, file)) {  
		       throw new IOException("Could not write an image of format " + format + " to " + file);  
		     }  
		}  
	    
	      
	   public static void writeToStream(BitMatrix matrix, String format, OutputStream stream)  
	       throws IOException {  
	     BufferedImage image = toBufferedImage(matrix);  
	     if (!ImageIO.write(image, format, stream)) {  
	       throw new IOException("Could not write an image of format " + format);  
	     }  
	   }  
	   
	   /**
	    * @Title: writeToStreamV2
	    * @Description: 添加颜色参数【rgb】
	    * @param @param matrix
	    * @param @param format
	    * @param @param stream
	    * @param @throws IOException   
	    * @return void    
	    * @throws
	    */
	   public static void writeToStreamV2(BitMatrix matrix, String format, OutputStream stream, int rgb)  
		       throws IOException {  
		     BufferedImage image = toBufferedImageV2(matrix, rgb);  
		     if (!ImageIO.write(image, format, stream)) {  
		       throw new IOException("Could not write an image of format " + format);  
		     }  
	   } 
	   
	   public static void main(String[] args) throws WriterException, IOException{
		   	 String content = "61f8acafcfa54bd5a07d054144cf403a123456123456";
		     String path = "C:/qrcode";
		     
		     MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
		     Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();  
		     hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		     hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);//容错率  L-7%, M-15%, Q-25%, H-30%
		     BitMatrix bitMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, 400, 400,hints);
		     File file1 = new File(path,"0525.jpg");
		     MatrixToImageWriter.writeToFileV2(bitMatrix, "jpg", file1, MatrixToImageWriter.BLUE);
	   }
}

