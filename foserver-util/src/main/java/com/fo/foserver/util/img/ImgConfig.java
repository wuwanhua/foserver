package com.fo.foserver.util.img;

import com.fo.foserver.util.base.StringUtil;

/**
 * 图片上传配置
 */
public class ImgConfig {
	
	public static String HTTP_PREFIX = "http";
	public static String APP_ID = "5p4m8Qyn-wvG4WYqzZBzJreETdJSoVbDgHahZqrm";
	public static String APP_SECRET = "9aVTwKN1sbOXi1Go0EaAPHBwc10a3vGNQOAwKZzz";
	public static String BUCKET_H5 = "h5-img";
	public static String CDN_PREFIX = "http://p0scd6705.bkt.clouddn.com/";
	
	/**
	 * 给url添加CDN域名 http://p0scd6705.bkt.clouddn.com/
	 * @param url
	 * @return
	 */
	public static String addCndImgUrl(String url, String extra, String defaultUrl) {

		if (StringUtil.isEmpty(url)) {
			return defaultUrl;
		}
		
		if (url.startsWith(HTTP_PREFIX)) {
			return url;
		}
		if (url.startsWith("/")) {
			url = url.substring(1);
		}

		return StringUtil.addString("", CDN_PREFIX, url, extra);
	}

}
