package com.fo.foserver.util.result;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Result<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2281372263714858950L;
	
	//调用成功/失败
	private boolean success;
	//消息code
	private String msgCode;
	//消息内容
	private String msgInfo;
	
	//list条数
	private Long count;
	
	
	//模板数据
	private T model;
	
	public Result() {
		this.success = true;
	}
	
	public Result(T model) {
		this.success = true;
		this.model = model;
	}
	
	public Result(MsgException e) {
		this.success = false;
		this.msgCode = e.getErrorCode();
		this.msgInfo = e.getErrorInfo();
	}
	public Result(MsgException e,boolean isSuccess) {
		this.success = isSuccess;
		this.msgCode = e.getErrorCode();
		this.msgInfo = e.getErrorInfo();
	}
	public Result(String msgInfo, MsgException e) {
		this.success = false;
		this.msgCode = e.getErrorCode();
		this.msgInfo = msgInfo;
	}
	
	

	public T getModel() {
		return model;
	}

	public void setModel(T model) {
		this.model = model;
	}
	
	public boolean isSuccess() {
		return this.success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgInfo() {
		return msgInfo;
	}
	public void setMsgInfo(String msgInfo) {
		this.msgInfo = msgInfo;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.DEFAULT_STYLE);
	}

}
