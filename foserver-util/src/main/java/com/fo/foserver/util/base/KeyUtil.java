package com.fo.foserver.util.base;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * 用于key值生成
 */
public class KeyUtil {
	
	/**
	 * 用户id  16位
	 * 获取16位ID, 支持分表256张
	 * 随机码 + 时间戳 + 分库码
	 * @return
	 */
	public synchronized static Long getUniqId() {
		
		//随机码
		long rand = NumberUtil.rand(1, 9);
		
		//时间戳
		long now = new Date().getTime();
		long time = now - 1503160000000L;
		DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
		df.applyPattern("000000000000");
		String timeStr = df.format(time);
		
		//分库码
		long mod = now % 256;
		df.applyPattern("000");
		String mode = df.format(mod);
		
		StringBuilder sb = new StringBuilder();
		sb.append(rand);    		       //1bit
		sb.append(timeStr);            //12bit
		sb.append(mode);     		   //3bit 
		return Long.valueOf(sb.toString());
	}
	
	/**
	 * 获取流水号 24bit
	 * 年月日时分秒 + 用户ID + 随机码 + 分库码
	 * @return
	 */
	public synchronized static String getSerialNumber(Long id) {
		
		//201708281553
		Date now = DateUtil.getNow();
		String timeStr = DateUtil.dateFormat(now, DateUtil.YYYYMMDDHHMMSS);
		
		//用户表的主键ID,  注意：不是user_id !!!
		DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
		df.applyPattern("00000000");
		String uid = df.format(id);
		
		//一位随机码
		long rand = NumberUtil.rand(1, 9);
		
		//分库分表预留位
		long mod = now.getTime() % 256;
		df.applyPattern("000");
		String mode = df.format(mod);
		
		StringBuilder sb = new StringBuilder();
		sb.append(timeStr);              //12bit
		sb.append(uid);			        //8bit  
		sb.append(rand);				    //1bit
		sb.append(mode);     		    //3bit 
		return sb.toString();
	}
	
	/**
	 * 获取合同编号
	 * @param id
	 * @return
	 */
//	public synchronized static String getContractNumber(Long id) {
//		
//		//20170828
//		Date now = DateUtil.getNow();
//		String timeStr = DateUtil.dateFormat(now, DateUtil.YYYYMMDD);
//		
//		//用户表的主键ID,  注意：不是user_id !!!
//		DecimalFormat df=(DecimalFormat) DecimalFormat.getInstance();
//		df.applyPattern("00000000");
//		String uid = df.format(id);
//		
//		//一位随机码
//		long rand = NumberUtil.rand(1, 9);
//		
//		//分库分表预留位
//		long mod = now.getTime() % 256;
//		df.applyPattern("000");
//		String mode = df.format(mod);
//		
//		StringBuilder sb = new StringBuilder();
//		sb.append(timeStr);              //8bit
//		sb.append(uid);			        //8bit  
//		sb.append(rand);				    //1bit
//		sb.append(mode);     		    //3bit 
//		return sb.toString();
//	}
	
	
}
