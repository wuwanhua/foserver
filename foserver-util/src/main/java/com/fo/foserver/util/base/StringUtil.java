package com.fo.foserver.util.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.util.result.Result;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class StringUtil {

	private static final Logger logger = LoggerFactory.getLogger(StringUtil.class);
	public static final String SPLIT = ",";
	public static final String SPLIT_END = "。";
	public static final String SPLIT_COMMA = ".";
	public static final String JSON_OPTION_ID = "optionId";
	public static final String JSON_OPTION_NAME = "optionName";
	public static final String JSON_OPTION_CONTENT = "optionContent";
	public static final String JSON_OPTION_DEFAULT = "optionDefault";
	public static final String JSON_OPTION_ICON = "optionIcon";
	public static final String JSON_CODE_ID = "codeId";
	public static final String JSON_CODE_NAME = "codeName";
	public static final String PRO_PROJECT_INFO_FILES = "projectDescFiles";   	//产品的项目说明文件地址信息
	public static final String PRO_PROJECT_INFO_FILES_URL = "fileUrl";   			//产品的项目说明文件地址
	public static final String JSON_TITLE = "title";   							//JSON内容
	public static final String JSON_CONTENT = "content";  						//JSON内容
	public static final String JSON_RIGHTS = "rightsContent";        					//合作权益
	public static final String JSON_REMAIN_PARTNER_NUM = "remainPartnerNum";        	//剩余合作人
	public static final String JSON_STATUS = "status";   			  					//状态
	public static final String JSON_DESC = "failReason";       			  			//说明
	public static final String LESS_AND_EQUAL = "≤";				//小于等于
	public static final String GREATER_AND_EQUAL = "≥";          //大于等于
	public static final String LESS = "<";						//小于
	public static final String GREATER = ">";					//大于
	public static final String REATE_INFO_X = "x";			    //金额范围
	public static final String SPLIT_FIX = "<x";                 //用于处理金额的格式化
	public static final String PERCENTAGE = "%";                  
	public static final String UNTI_PEOPLE = "人";
	
	/**
	 * StringUtils 判断某个字符在目标字符串中出现的次数
	 *
	 * @param str
	 *            目标字符串
	 * @param con
	 *            字符
	 * @return int 次数
	 */
	public static int numberOfStr(String str, char con) {
		String string = String.valueOf(con);
		return isEmpty(str) || isEmpty(string) ? 0 : (str.length() - str.replace(string, "").length()) / string.length();
	}

	/**
	 * 判断某个字符在目标字符串中出现的次数
	 *
	 * @param str
	 *            目标字符串
	 * @param con
	 *            字符串
	 * @return int 次数
	 */
	public static int numberOfStr(String str, String con) {
		if (isEmpty(str) || isEmpty(con)) {
			return 0;
		}
		int i = 0;
		while (str.indexOf(con) != -1) {
			str = replace(str, str.indexOf(con), "");
			i++;
		}
		return i;
	}

	/**
	 * 全角字符串转换半角字符串
	 *
	 * @param s
	 *            全角字符串
	 * @return String 半角字符串
	 */
	public static String fullWidthToHalfWidth(String s) {
		if (isEmpty(s)) {
			return s;
		}
		char[] source = s.toCharArray();
		for (int i = 0; i < source.length; i++) {
			source[i] = source[i] == 12288 ? ' '
					: source[i] >= 65281 && source[i] <= 65374 ? (char) (source[i] - 65248) : source[i];
		}
		return new String(source);
	}

	/**
	 * 半角字符串转换全角字符串
	 *
	 * @param s
	 *            半角字符串
	 * @return String 全角字符串
	 */
	public static String halfWidthToFullWidth(String s) {
		if (isEmpty(s)) {
			return s;
		}
		char[] source = s.toCharArray();
		for (int i = 0; i < source.length; i++) {
			source[i] = source[i] == ' ' ? (char) 12288
					: source[i] >= 33 && source[i] <= 126 ? (char) (source[i] + 65248) : source[i];
		}
		return new String(source);
	}

	/**
	 * 首字母大写
	 *
	 * @param str
	 *            {@code str}
	 * @return String 首字母大写
	 */
	public static String capitalizeFirstLetter(String str) {
		if (isEmpty(str)) {
			return str;
		}
		char c = str.charAt(0);
		return (!Character.isLetter(c) || Character.isUpperCase(c)) ? str
				: new StringBuilder(str.length()).append(Character.toUpperCase(c)).append(str.substring(1)).toString();
	}

	/**
	 * 替换字符串中的指定字符串
	 *
	 * @param str
	 *            要替换的字符串
	 * @param oldStr
	 *            原字符,为空相当于追加字符
	 * @param newStr
	 *            替换字符,为空相当于删除字符
	 * @return String 替换后的字符串
	 */
	public static String replace(String str, String oldStr, String newStr) {
		if (isEmpty(str)) {
			return "";
		}
		if (isEmpty(oldStr)) {
			return str + newStr;
		}
		if (isEmpty(newStr)) {
			newStr = "";
		}
		int index = str.indexOf(oldStr);
		if (index > str.length() || index < 0) {
			return str;
		}
		int oldLength = oldStr.length();
		String begin = str.substring(0, index);
		String end = str.substring(index + oldLength, str.length());
		return begin + newStr + end;
	}

	/**
	 * 替换指定位置的字符
	 *
	 * @param str
	 *            要替换的字符串
	 * @param index
	 *            位置
	 * @param newStr
	 *            替换字符,为空时相当于删除指定位置的字符
	 * @return String 替换后的字符串
	 */
	public static String replace(String str, int index, String newStr) {
		if (isEmpty(str)) {
			return "";
		}
		if (index > str.length() || index < 0) {
			return str;
		}
		if (isEmpty(newStr)) {
			newStr = "";
		}
		String begin = str.substring(0, index);
		String end = str.substring(index + 1, str.length());
		return begin + newStr + end;
	}

	/**
	 * 检测是否有emoji字符
	 *
	 * @param source
	 *            要判断的字符串
	 * @return 一旦含有就抛出true
	 */
	public static boolean containsEmoji(String source) {
		if (isEmpty(source)) {
			return false;
		}
		int len = source.length();

		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (isEmojiCharacter(codePoint)) {
				// do nothing，判断到了这里表明，确认有表情字符
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断字符是否是emoji字符
	 *
	 * @param codePoint
	 *            字符
	 * @return true or false
	 */
	private static boolean isEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD)
				|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
				|| ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}

	/**
	 * 转换成Unicode码
	 *
	 * @param text
	 *            要转换的字符串
	 * @param charset
	 *            转换之前的字符集
	 * @return Unicode码String ,返回-1为字符集无效,返回-2为io异常
	 *
	 */
	public static String toUnicode(String text, String charset) {
		String result = "";
		int input;
		StringReader isr;
		try {
			isr = new StringReader(new String(text.getBytes(), charset));
		} catch (UnsupportedEncodingException e) {
			return "-1";
		}
		try {
			while ((input = isr.read()) != -1) {
				result = result + "&#x" + Integer.toHexString(input) + ";";
			}
		} catch (IOException e) {
			logger.error("", e);
			return "-2";
		}
		isr.close();
		return result;
	}

	/**
	 * 转换成MD5码
	 *
	 * @param str
	 *            要转换的字符串
	 * @return MD5码
	 */
	public static String toMD5(String str) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] strTemp = getBytes(str);
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(strTemp);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char chart[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				chart[k++] = hexDigits[byte0 >>> 4 & 0xf];
				chart[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(chart);
		} catch (Exception e) {
			logger.error("", e);
			return null;
		}
	}

	/**
	 * 16位加密
	 * @param str
	 * @return
	 */
	public static String toMD16(String str) {
		String result = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			result = buf.toString().substring(8, 24);
		} catch (Exception e) {
			logger.error("", e);
			return null;
		}
		return result;
	}

	/**
	 * 判断输入的字节数组是否为空
	 *
	 * @return boolean 空则返回true,非空则flase
	 * @param bytes
	 *            字节数组
	 */
	public static boolean isEmpty(byte[] bytes) {
		return null == bytes || 0 == bytes.length;
	}

	/**
	 * 字节数组转为字符串
	 *
	 * @see 该方法默认以UTF-8转码
	 * @see 若想自己指定字符集,可以使用<code>getBytes(String str, String charset)</code>方法
	 * @param bs
	 *            字节数组
	 * @return 字符串
	 */
	public static String getString(byte[] bs) {
		return getString(bs, "UTF-8");
	}

	/**
	 * 字节数组转字符串
	 *
	 * @see 如果系统不支持所传入的<code>charset</code>字符集,则按照系统默认字符集进行转换
	 * @param bs
	 *            字节数组
	 * @param charset
	 *            指定编码
	 * @return 字符串
	 */
	public static String getString(byte[] bs, String charset) {
		if (isEmpty(bs)) {
			return "";
		}
		try {
			return new String(bs, charset);
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
			return new String(bs);
		}
	}

	/**
	 * 字符串转为字节数组
	 *
	 * @see 该方法默认以UTF-8转码
	 * @see 若想自己指定字符集,可以使用<code>getBytes(String str, String charset)</code>方法
	 * @param data
	 *            字符串
	 * @return 字节数组
	 */
	public static byte[] getBytes(String data) {
		return getBytes(data, "UTF-8");
	}

	/**
	 * 字符串转为字节数组
	 *
	 * @see 如果系统不支持所传入的<code>charset</code>字符集,则按照系统默认字符集进行转换
	 * @param data
	 *            字符串
	 * @param charset
	 *            编码
	 * @return 字节数组
	 */
	public static byte[] getBytes(String data, String charset) {
		data = (data == null ? "" : data);
		if (isEmpty(charset)) {
			return data.getBytes();
		}
		try {
			return data.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
			return data.getBytes();
		}
	}

	/**
	 *
	 * 字符串转布尔值
	 *
	 * @param str
	 *            字符串
	 * @return true or false 无法转换返回false
	 */
	public static boolean toBoolValue(String str) {
		return Boolean.parseBoolean(str);
	}

	/**
	 * 字符串转长整数
	 *
	 * @param str
	 * @param defValue
	 *            默认值
	 * @return 转换异常返回默认值
	 */
	public static long toLong(String str, long defValue) {
		if (NumberUtil.isLong(str)) {
			return Long.parseLong(str);
		}
		return defValue;
	}

	/**
	 * 字符串转长整数,默认返回0
	 *
	 * @param str
	 * @return 转换异常返回 0
	 */
	public static long toLong(String str) {
		return toLong(str, 0);
	}

	public static void main(String[] args) {
		//		System.out.println(StringUtils.isBlank("   "));
		//		System.out.println(StringUtil.isEmpty("   "));
		Date date = StringUtil.toDate("2016-09-09 23:49:39", DateUtil.YYYY_MM_DD_HH_MM_SS, new Date());
		System.out.println(date);

	}

	/**
	 * 字符串转整数
	 *
	 * @param str
	 *            字符串
	 * @param defValue
	 *            无法转换或异常时返回的默认值
	 * @return 整数
	 */
	public static Integer toIntValue(String str, int defValue) {
		if (NumberUtil.isInteger(str)) {
			return Integer.parseInt(str);
		}
		return defValue;
	}

	/**
	 * 字符串转整数
	 *
	 * @param str
	 *            字符串
	 * @return 转换异常返回 0
	 */
	public static Integer toIntValue(String str) {
		return toIntValue(str, 0);
	}

	/**
	 * 字符串转Double
	 *
	 * @param str
	 *            字符串
	 * @param defValue
	 *            无法转换或异常时返回的默认值
	 * @return 整数
	 */
	public static Double toDoulbeValue(String str, double defValue) {
		if (NumberUtil.isDouble(str)) {
			return Double.parseDouble(str);
		}
		return defValue;
	}

	/**
	 * 字符串转Double
	 *
	 * @param str
	 *            字符串
	 * @return 出错返回0
	 */
	public static Double toDoulbeValue(String str) {
		return toDoulbeValue(str, 0);
	}

	/**
	 * 字符串转日期
	 *
	 * @see 如果发生格式化异常返回yyyy-MM-dd日期
	 * @see 如果发生异常返回默认日期
	 *
	 * @param str
	 *            字符串
	 * @param pattern
	 *            字符串格式
	 * @param defDate
	 *            默认时间
	 * @return 日期
	 */
	public static Date toDate(String str, String pattern, Date defDate) {
		try {
			return new SimpleDateFormat(pattern).parse(str);
		} catch (ParseException e) {
			logger.error("", e);
			try {
				return new SimpleDateFormat("yyyy-MM-dd").parse(str);
			} catch (ParseException e1) {
				logger.error("", e1);
				return defDate;
			}
		}
	}

	/**
	 * 字符串转时间
	 *
	 * @see 如果发生格式化异常返回yyyy-MM-dd HH:mm:ss日期
	 * @see 如果发生异常返回默认时间
	 * @param str
	 *            字符串
	 * @param pattern
	 *            字符串格式
	 * @param defDate
	 *            默认时间
	 * @return 时间
	 */
	public static Date toTime(String str, String pattern, Date defDate) {
		try {
			return new SimpleDateFormat(pattern).parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			try {
				return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
			} catch (ParseException e1) {
				logger.error("", e1);
				return defDate;
			}
		}
	}

	/**
	 * 以yyyy-MM-dd格式将字符串转日期
	 *
	 * @see 如果发生异常返回默认日期
	 *
	 * @param str
	 *            字符串
	 * @param defDate
	 *            默认时间
	 * @return 日期
	 */
	public static Date toDate(String str, Date defDate) {
		return toDate(str, "yyyy-MM-dd", defDate);
	}

	/**
	 * 以yyyy-MM-dd HH:mm:ss格式将字符串转日期
	 *
	 * @see 如果发生异常返回默认时间
	 * @param str
	 *            字符串
	 * @param defDate
	 *            默认时间
	 * @return 时间
	 */
	public static Date toTime(String str, Date defDate) {
		return toTime(str, "yyyy-MM-dd HH:mm:ss", defDate);
	}

	/**
	 * 判断给定字符串是否空白串。 空白串是指由空格、制表符、回车符、换行符组成的字符串 若输入字符串为null或空字符串，返回true
	 *
	 * @param input
	 *            给定字符串
	 * @return true or false
	 */
	public static boolean isEmpty(String input) {
		if (input == null || "".equals(input) || input.length() == 0) {
			return true;
		}
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotEmpty(String input) {
		return !isEmpty(input);
	}

	/**
	 * 去除字符串中所有空格
	 *
	 * @see 也可以使用正则表达式去除空格的方法JeffHtmlUtils.delSpaceAndEnterTag(String str);
	 *
	 * @param input
	 *            字符串
	 * @return 去除字所有空格的字符串
	 */
	public static String removeSpace(String input) {
		return removeSelf(input, " ", "\t", "\n", "\r");
	}

	/**
	 * 保留指定字符或字符串,其他删除
	 *
	 * @see 保留结果,如"abcde",保留"ab","bc"结果为"abc"
	 *
	 * @param input
	 *            目标字符串
	 * @param c
	 *            标记字符,请不要与str中的首字符重复,也不要使用空格
	 * @param str
	 *            要保留的字符串或者字符串数组
	 * @return 去除其他字符后的{@code str}
	 * @throws Exception
	 */
	public static String removeOther(String input, char c, String... str) throws Exception {
		List<Integer> list = new ArrayList<Integer>();
		if (isEmpty(input) || ArrayUtil.isEmpty(str)) {
			return input;
		}
		for (String string : str) {
			if (isEmpty(string)) {
				continue;
			}
			list.addAll(getIndexInString(input, string, c));
		}
		list = ArrayUtil.removeDuplicate(list);
		String newStr = "";
		for (int i = 0; i < list.size(); i++) {
			newStr += input.charAt(list.get(i));
		}
		return newStr;
	}

	/**
	 *
	 * 获得str在input中所占的所有字符位置
	 *
	 * @param input
	 *            目标字符串
	 * @param str
	 *            要判断的字符串
	 * @param c
	 *            标记字符,请不要与str中的首字符重复,也不要使用空格
	 * @return str首字母在input中所占的所有字符位置
	 * @throws Exception
	 */
	private static List<Integer> getIndexInString(String input, String str, char c) throws Exception {
		List<Integer> list = new ArrayList<Integer>();
		if (isEmpty(input) || isEmpty(str) || !input.contains(str)) {
			return list;
		}
		if (str.indexOf(c) == 0 || isEmpty(String.valueOf(c))) {
			throw new Exception("标记字符出错,请不要与str中的首字符重复,也不要使用空格");
		}
		int i = -1;
		while ((i = input.indexOf(str)) != -1) {
			input = replace(input, i, String.valueOf(c));
			for (int j = 0; j < str.length(); j++) {
				list.add(i + j);
			}
		}
		return ArrayUtil.removeDuplicate(list);
	}

	/**
	 * 去除指定字符或字符串并去除所有空格
	 *
	 * @see 例:"abcdef"中去除"bc","cd"的结果为"adf",如需要得到"adef",请使用removeSelf()方法
	 * @param input
	 *            目标字符串
	 * @param str
	 *            要去除的字符或者字符串数组
	 * @return 去除{@code str}后的{@code input}
	 */
	public static String removeSelfPerfer(String input, String... str) {
		return removeSpace(removeSelfPerfer(input, ' ', str));
	}

	/**
	 * 去除指定字符或字符串
	 *
	 * @see 例:"abcdef"中去除"bc","cd"的结果为"adf",如需要得到"adef",请使用removeSelf()方法
	 * @param input
	 *            目标字符串
	 * @param tag
	 *            调整符,请选择字符串中没有出现的字符
	 * @param str
	 *            要去除的字符或者字符串数组
	 * @return 去除{@code str}后的{@code input}
	 */
	public static String removeSelfPerfer(String input, char tag, String... str) {
		if (ArrayUtil.isEmpty(str) || input == null) {
			return input;
		}
		List<char[]> list = new ArrayList<char[]>();
		char[] charArr = input.toCharArray();
		for (String string : str) {
			if (isEmpty(string)) {
				continue;
			}
			String space = "";
			for (int i = 0; i < string.length(); i++) {
				space += tag;
			}
			list.add(input.replaceAll(string, space).toCharArray());
		}
		for (int i = 0; i < input.length(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if (list.get(j)[i] != charArr[i])
					charArr[i] = tag;
			}
		}
		return removeSelf(new String(charArr), String.valueOf(tag));
	}

	/**
	 * 去除指定字符或字符串
	 *
	 * @see 例:"abcdef"中去除"bc","cd"的结果为"adef",如需要得到"aef",请使用removeSelfPerfer()方法
	 * @param input
	 *            目标字符串
	 * @param str
	 *            要去除的字符或者字符串数组
	 * @return 去除{@code str}后的{@code input}
	 */
	public static String removeSelf(String input, String... str) {
		for (String string : str) {
			input = input.replaceAll(string, "");
		}
		return input;
	}

	/**
	 * 去除字符串中所有数字
	 * @param input
	 * @return
	 */
	public static String removeNumber(String input) {

		if (isEmpty(input)) {
			return "";
		}

		return removeSelf(input, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", " ", "\t", "\n", "\r");
	}

	/**
	 * 拼接字符串
	 *
	 * @param tag
	 *            连接符
	 * @param str
	 *            需要拼接的字符串,可多选
	 * @return 连接后的字符串
	 */
	public static String addString(String tag, String... str) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < str.length; i++) {
			sb.append(i == str.length - 1 ? str[i] : str[i] + tag);
		}
		return sb.toString();
	}

	/**
	 * 以utf-8进行字符编码
	 *
	 * @see 该方法通常用于对中文进行UTF-8编码
	 * @see 若系统不支持指定的编码字符集,则直接将<code>chinese</code>原样返回
	 * @param chinese
	 *            要编码的字符串
	 * @return 编码后的字符串
	 */
	public static String encode(String chinese) {
		return encode(chinese, "UTF-8");
	}

	/**
	 * 字符编码
	 *
	 * @see 该方法通常用于对中文进行编码
	 * @see 若系统不支持指定的编码字符集,则直接将<code>chinese</code>原样返回
	 * @param chinese
	 *            要编码的字符串
	 * @param charset
	 *            编码规则
	 * @return 编码后的字符串
	 */
	public static String encode(String chinese, String charset) {
		if (chinese == null) {
			return null;
		}
		try {
			return URLEncoder.encode(chinese, charset);
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
			return chinese;
		}
	}

	/**
	 * 以utf-8进行字符解码
	 *
	 * @see 该方法默认会以UTF-8解码字符串
	 * @see 若想自己指定字符集,可以使用<code>decode(String chinese, String charset)</code>方法
	 * @param chinese
	 *            字符串
	 * @return 解码后的字符串
	 */
	public static String decode(String chinese) {
		return decode(chinese, "UTF-8");
	}

	/**
	 * 字符解码
	 *
	 * @see 该方法通常用于对中文进行解码
	 * @see 若系统不支持指定的解码字符集,则直接将<code>chinese</code>原样返回
	 * @param chinese
	 *            字符串
	 * @param charset
	 *            解码规则
	 * @return 解码后的字符串
	 */
	public static String decode(String chinese, String charset) {
		if (chinese == null) {
			return null;
		}
		try {
			return URLDecoder.decode(chinese, charset);
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
			return chinese;
		}
	}

	/**
	 * 字符串转码
	 *
	 * @param srcCode
	 *            原编码
	 * @param destCode
	 *            目标编码
	 * @param strTmp
	 *            字符串
	 * @return 为null时返回""
	 */
	public static String convertCode(String srcCode, String destCode, String strTmp) {
		if (strTmp == null) {
			return null;
		}
		try {
			byte[] ascii = strTmp.getBytes(srcCode);
			strTmp = new String(ascii, destCode);
			return strTmp;
		} catch (Exception e) {
			logger.error("", e);
			return strTmp;
		}
	}

	/**
	 * 创建指定数量的随机字符串
	 *
	 * @param numberFlag
	 *            是否是数字
	 * @param length
	 * @return
	 */
	public static String createRandom(boolean numberFlag, int length) {
		String retStr = "";
		String strTable = numberFlag ? "1234567890" : "1234567890abcdefghijkmnpqrstuvwxyz";
		int len = strTable.length();
		boolean bDone = true;
		do {
			retStr = "";
			int count = 0;
			for (int i = 0; i < length; i++) {
				double dblR = Math.random() * len;
				int intR = (int) Math.floor(dblR);
				char c = strTable.charAt(intR);
				if (('0' <= c) && (c <= '9')) {
					count++;
				}
				retStr += strTable.charAt(intR);
			}
			if (count >= 2) {
				bDone = false;
			}
		} while (bDone);

		return retStr;
	}

	/**
	 * 将InputStream转换成某种字符编码的String
	 * @param in
	 * @param encoding
	 * @return
	 * @throws Exception
	 */
	public static String inputStreamTOString(InputStream in, String encoding) {

		try {
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, encoding));
			//最好在将字节流转换为字符流的时候 进行转码  
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = bf.readLine()) != null) {
				buffer.append(line);
			}

			return buffer.toString();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;

	}

	/**
	 * 如果字符串为空返回null
	 * @param temp
	 * @return
	 */
	public static String isEmptyReturnNull(String temp) {

		if (isEmpty(temp)) {
			return null;
		} else {
			return temp;
		}

	}

	/**
	 * 如果字符串为空返回null
	 * @param temp
	 * @return
	 */
	public static Integer isEmptyReturnIntegerNull(String temp) {

		if (isEmpty(temp)) {
			return null;
		} else {
			return Integer.valueOf(temp);
		}

	}

	/**
	 * 在html页面显示\n\r这类标识
	 *
	 * @param str
	 * @return
	 */
	public static String showBR(String str) {
		if (StringUtils.isNotBlank(str)) {
			str = str.replaceAll("\n", "<br>");
			str = str.replaceAll(" ", "&nbsp;");
		}
		return str;
	}

	/**
	 * @Title: isNullReturnEmptyString
	 * @Description: 字符串为空返回空字符串
	 * @param @param temp
	 * @param @return   
	 * @return String    
	 * @throws
	 */
	public static String isNullReturnEmptyString(String temp) {

		if (isEmpty(temp)) {
			return StringUtils.EMPTY;
		} else {
			return temp;
		}
	}

	/**
	 * @Title: getMapKeys
	 * @Description: 获取Map中所有key
	 * @param @param map
	 * @param @return   
	 * @return List<String>    
	 * @throws
	 */
	public static List<String> getMapKeys(Map<String, JSONObject> map) {

		List<String> li = new ArrayList<String>();
		if (map == null || map.size() < 1) {
			return li;
		}

		List<String> list = new ArrayList<String>();
		for (Map.Entry<String, JSONObject> entry : map.entrySet()) {
			list.add(entry.getKey());
		}
		return list;
	}

	/**
	 * @Title: getMapValues
	 * @Description: 获取Map中所有value
	 * @param @param map
	 * @param @return   
	 * @return List<String>    
	 * @throws
	 */
	public static List<String> getMapValues(Map<String, JSONObject> map) {

		List<String> li = new ArrayList<String>();
		if (map == null || map.size() < 1) {
			return li;
		}

		List<String> list = new ArrayList<String>();
		for (Map.Entry<String, JSONObject> entry : map.entrySet()) {

			JSONObject b = null;
			try {
				b = JSONObject.fromObject(entry.getValue());
			} catch (Throwable e) {
				continue;
			}

			if (b == null || !b.containsKey("parentName") || !b.containsKey("name")) {
				continue;
			}

			list.add(StringUtil.addString("", b.optString("parentName"), "/", b.optString("name")));
		}
		return list;
	}

	/**
	 * @Title: getMsgInfo
	 * @Description: 当Result异常信息为空的时候，获取msgInfo
	 * @param @param result
	 * @param @param msgInfo
	 * @param @return   
	 * @return String    
	 * @throws
	 */
	public static <T> String getMsgInfo(Result<T> result, String msgInfo) {

		if (result == null || StringUtil.isEmpty(result.getMsgInfo())) {
			return msgInfo;
		}
		return result.getMsgInfo();
	}

	/**
	 * @Title: idsToList
	 * @Description: 将ids转为列表
	 * @param @param ids
	 * @param @return   
	 * @return List<Long>    
	 * @throws
	 */
	public static List<Long> idsToList(String ids) {

		List<Long> list = new ArrayList<Long>();

		//vendorId为合作商ID的集合  存储格式:   ;  1;  1,2,3;
		if (StringUtil.isEmpty(ids)) {
			return list;
		}

		if (ids.contains(",")) {
			for (String id : ids.split(",")) {
				list.add(StringUtil.toLong(id, 0L));
			}
		} else {
			list.add(StringUtil.toLong(ids, 0L));
		}
		return list;
	}

	/**
	 * @Title: subStringLastChar
	 * @Description: 去除字符串最后一个符号
	 * @param @param str
	 * @param @return   
	 * @return String    
	 * @throws
	 */
	public static String subStringLastChar(String str, String charStr) {

		String result = StringUtils.EMPTY;
		if (str.length() > 0 && str.toString().lastIndexOf(charStr) == str.toString().length() - 1) {
			result = str.toString().substring(0, str.toString().length() - 1);
		} else {
			result = str.toString();
		}
		return result;
	}

	/**
	 * @Title: toIntIsEmptyReturnNull
	 * @Description: 如果参数为空返回null
	 * @param @param str
	 * @param @return   
	 * @return Integer    
	 * @throws
	 */
	public static Integer toIntIsEmptyReturnNull(String str) {

		if (StringUtil.isEmpty(str)) {
			return null;
		}

		return StringUtil.toIntValue(str);
	}

	/**
	 * @Title: toLongIsEmptyReturnNull
	 * @Description:  如果参数为空返回null
	 * @param @param str
	 * @param @return   
	 * @return Long    
	 * @throws
	 */
	public static Long toLongIsEmptyReturnNull(String str) {

		if (StringUtil.isEmpty(str)) {
			return null;
		}

		return StringUtil.toLong(str);
	}

	/**
	 * @Title: toDateIsEmptyReturnNull
	 * @Description:  如果参数为空返回null
	 * @param @param date
	 * @param @param dateFormat
	 * @param @return   
	 * @return Date    
	 * @throws
	 */
	public static Date toDateIsEmptyReturnNull(String date, String dateFormat) {

		if (StringUtil.isEmpty(date)) {
			return null;
		}

		return DateUtil.dateFormat(date, dateFormat);

	}

	/**
	 * @Title: isSuccess
	 * @Description: 处理结果判断
	 * @param @param result(String, Long, Integer)
	 * @param @return   
	 * @return Boolean    
	 * @throws
	 */
	public static Boolean isSuccess(Result<?> result) {
		if (result == null || !result.isSuccess() || result.getModel() == null) {
			return false;
		}
		
		Object ob = result.getModel();
		String className = ob.getClass().getName();
		if(className.equals(Boolean.class.getName()) && !(Boolean)ob){
			return false;
		}
		if(className.equals(String.class.getName()) && StringUtil.isEmpty(ob.toString())){
			return false;
		}
		if(className.equals(Long.class.getName()) && (Long)(Long.parseLong(ob.toString())) < 1L){
			return false;
		}
		if(className.equals(Integer.class.getName()) && (Integer)Integer.parseInt(ob.toString()) < 1){
			return false;
		}
		return true;
	}

	/**
	 * @Title: objectToString
	 * @Description: 对象转字符串
	 * @param @param var
	 * @param @return   
	 * @return String    
	 * @throws
	 */
	public static String objectToString(Object var) {

		if (ObjectUtil.isEmpty(var)) {
			return StringUtils.EMPTY;
		}

		return var.toString();
	}

	/**
	 * @Title: isEmptyLong
	 * @Description: 判断Long类型数据是否为空
	 * @param @param num
	 * @param @return   
	 * @return boolean    
	 * @throws
	 */
	public static boolean isEmptyLong(Long num) {
		return num == null ? true : (num < 1L ? true : false);
	}

	/**
	 * @Title: isEmptyBigDecimal
	 * @Description: 判断BigDecimal类型数据是否为空
	 * @param @param num
	 * @param @return   
	 * @return boolean    
	 * @throws
	 */
	public static boolean isEmptyBigDecimal(BigDecimal amt) {
		return amt == null ? true : (amt.compareTo(Money.AMT_ZERO) <= 0 ? true : false);
	}

	/**
	 * @Title: isEmptyInteger
	 * @Description: 判断Integer类型数据是否为空
	 * @param @param num
	 * @param @return   
	 * @return boolean    
	 * @throws
	 */
	public static boolean isEmptyInteger(Integer num) {
		return num == null ? true : (num < 1 ? true : false);
	}

	/**
	 * Integer为空返回null
	 * @param num
	 * @return
	 */
	public static String isEmptyIntegerReturnNull(Integer num) {
		return isEmptyInteger(num) ? null : num.toString();
	}

	/**
	 * Integer为空返回null
	 * @param num
	 * @return
	 */
	public static Integer isEmptyIntegerReturnIntegerNull(Integer num) {
		return isEmptyInteger(num) ? null : num;
	}

	/**
	 * @Title: isEmptyObject
	 * @Description: 判断对象是否为空
	 * @param @param ob
	 * @param @return   
	 * @return boolean    
	 * @throws
	 */
	public static boolean isEmptyObject(Object ob) {
		return ob == null ? true : false;
	}

	/**
	 * 将字符串转JSONObject
	 * @param json
	 * @return
	 */
	public static JSONObject strToJSONObject(String json) {

		JSONObject js = new JSONObject();
		if (StringUtil.isEmpty(json)) {
			return js;
		}

		try {
			return JSONObject.fromObject(json);
		} catch (Throwable e) {
			return js;
		}
	}

	/**
	 * 将字符串转JSONArray
	 * @param json
	 * @return
	 */
	public static JSONArray strToJSONArray(String json) {

		if (StringUtil.isEmpty(json)) {
			return null;
		}

		try {
			return JSONArray.fromObject(json);
		} catch (Throwable e) {
			return null;
		}
	}

	/**
	 * 将字符串转JSONArray, 为空返回空数据
	 * @param json
	 * @return
	 */
	public static JSONArray strToJSONArrayIsEmptyReturnArr(String json){
		
		JSONArray arr = new JSONArray();
		if(StringUtil.isEmpty(json)){
			return arr;
		}
		
		try{
			return JSONArray.fromObject(json);
		}catch(Throwable e){
			return arr;
		}
	}
	
	/**
	 * JSONArray转Map   (JSON格式: [{"1": "A"},{"2": "B"}])
	 * @param json
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Map<String, String> jsonArrayToMap(String json) {

		if (StringUtil.isEmpty(json)) {
			return null;
		}

		JSONArray arr = strToJSONArray(json);
		if (arr == null || arr.size() < 1) {
			return null;
		}

		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < arr.size(); i++) {

			JSONObject js = (JSONObject) arr.get(i);
			if (js == null) {
				return null;
			}

			for (Iterator tem = js.keys(); tem.hasNext();) {
				String key = (String) tem.next();
				if (!map.containsKey(key)) {
					map.put(key, js.optString(key));
				}
			}
		}

		return map;
	}

	/**
	 * 判断Long为空返回0
	 * @param num
	 * @return
	 */
	public static Long isEmpytLongReturnZero(Long num) {
		return StringUtil.isEmptyLong(num) ? 0L : num;
	}

	/**
	 * 判断Integer为空返回0
	 * @param num
	 * @return
	 */
	public static Integer isEmpyIntegerReturnZero(Integer num) {
		return StringUtil.isEmptyInteger(num) ? 0 : num;
	}
	
	/**
	 * 获取分页page
	 * @param page
	 * @return
	 */
	public static Integer getPage(Integer page) {

		if (StringUtil.isEmptyInteger(page)) {
			return 1;
		}

		return page;
	}

	/**
	 * 获取分页位移
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public static Integer getOffset(Integer page, Integer pageSize) {

		if (StringUtil.isEmptyInteger(pageSize) || pageSize > 100) {
			pageSize = 20;
		}

		page = getPage(page);
		Integer offset = (page - 1) * pageSize;
		return offset;
	}

	/**
	 * 将字符串根据指定字符，分割后转成列表
	 * @param str
	 * @return
	 */
	public static List<String> splitStr(String str, String split) {

		if (StringUtil.isEmpty(str) || StringUtil.isEmpty(split)) {
			return null;
		}

		String[] arr = str.split(split);
		List<String> list = Arrays.asList(arr);
		return list;
	}

	/**
	 * 将字符串根据指定字符，分割后转成列表
	 * @param str
	 * @return
	 */
	public static List<Long> splitIds(String str, String split) {

		if (StringUtil.isEmpty(str) || StringUtil.isEmpty(split)) {
			return null;
		}

		List<Long> list = new ArrayList<Long>();
		String[] arr = str.split(split);
		for (int i = 0; i < arr.length; i++) {
			if (StringUtil.isEmpty(arr[i])) {
				continue;
			}
			list.add(Long.valueOf(arr[i]));
		}
		return list;
	}
	public static List<Integer> splitIntegerIds(String str, String split) {

		if (StringUtil.isEmpty(str) || StringUtil.isEmpty(split)) {
			return null;
		}

		List<Integer> list = new ArrayList<Integer>();
		String[] arr = str.split(split);
		for (int i = 0; i < arr.length; i++) {
			if (StringUtil.isEmpty(arr[i])) {
				continue;
			}
			list.add(Integer.valueOf(arr[i]));
		}
		return list;
	}

	/**
	 * 根据手机号生成昵称
	 * @param mobile
	 * @return
	 */
	public static String getNicknameByMobile(String mobile) {

		if (StringUtil.isEmpty(mobile) || !ValidateUtil.isMobile(mobile)) {
			return StringUtils.EMPTY;
		}

		//13655812763
		String first = mobile.substring(0, 3);
		String last = mobile.substring(7);
		return StringUtil.addString("", first, "****", last);
	}

	/**
	 * 将list转枚举
	 * @param list
	 * @return
	 */
	public static String listToStr(List<Integer> list, String split) {

		if (ArrayUtil.isEmpty(list)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (Integer ob : list) {
			if (ob == null) {
				continue;
			}
			sb.append(ob.toString());
			sb.append(split);
		}

		return subStringLastChar(sb.toString(), split);
	}

	/**
	 * 昵称为空，显示手机号后六位
	 * @param nickname
	 * @param mobile
	 * @return
	 */
	public static String nicknameIsEmptyReturnMobile(String nickname, String mobile) {

		if (!StringUtil.isEmpty(nickname)) {
			return nickname;
		}

		if (StringUtil.isEmpty(mobile) || !ValidateUtil.isMobile(mobile)) {
			return StringUtils.EMPTY;
		}

		//13655812763
		return mobile.substring(5);

	}

	/**
	 * 去除运算符号
	 * @param amt
	 * @return
	 */
	public static String amtRateInfo(String amt) {

		if (StringUtil.isEmpty(amt)) {
			return StringUtils.EMPTY;
		}
		amt = removeSpace(amt);
		amt = replace(amt, LESS_AND_EQUAL, "");
		amt = replace(amt, GREATER_AND_EQUAL, "");
		amt = replace(amt, LESS, "");
		amt = replace(amt, GREATER, "");
		return amt;
	}

	/**
	 * 去除运算符号
	 * @param amt
	 * @return
	 */
	public static String amtRateInfoReplace(String amt) {

		if (StringUtil.isEmpty(amt)) {
			return StringUtils.EMPTY;
		}
		amt = removeSpace(amt);
		amt = amt.replaceAll(LESS_AND_EQUAL, "");
		amt = amt.replaceAll(GREATER_AND_EQUAL, "");
		amt = amt.replaceAll(LESS, "");
		amt = amt.replaceAll(GREATER, "");
		return amt;
	}

	/**
	 * 将版本号转数字
	 * @param version
	 * @return
	 */
	public static Long getVersionNum(String version) {

		if (!version.contains(StringUtil.SPLIT_COMMA)) {
			return null;
		}

		//将版本号转为数字(缺位用0补齐)  xxxxx.xxxxx.xxxxx
		version = StringUtil.removeSpace(version);
		String[] ver = version.split("\\.");
		if (ver == null || ver.length != 3) {
			return null;
		}

		//转数字
		DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
		df.applyPattern("00000");

		StringBuilder st = new StringBuilder();
		st.append(df.format(StringUtil.toLong(ver[0])));
		st.append(df.format(StringUtil.toLong(ver[1])));
		st.append(df.format(StringUtil.toLong(ver[2])));
		Long verNum = StringUtil.toLongIsEmptyReturnNull(st.toString());
		if (StringUtil.isEmptyLong(verNum)) {
			return null;
		}

		return verNum;
	}

	/**
	 * 如果小于0返回0
	 * @param num
	 * @return
	 */
	public static Integer getRemainNum(Integer num) {

		if (num == null || num < 0) {
			return 0;
		}
		return num;
	}

}
