package com.fo.foserver.util.base;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 算数工具类
 */
public class ArithmeticUtil {
	
	/**
	 * BigDecimal-roundingMode(舍入模式)参数说明
	 * 
	 * 1, ROUND_UP           只要舍弃部分不为0, 即前面的数字加1  						例如：5.5= 6, -1.0= -1
	 * 2, ROUND_DOWN         直接去除舍弃部分               					    例如：1.6= 1, -1.0= -1
	 * 3, ROUND_CEILING      正数：ROUND_UP;   负数：ROUND_DOWN   					例如：2.5= 3, -1.6= -1
	 * 4, ROUND_FLOOR        正数：ROUND_DOWN  负数：ROUND_UP     					例如：2.5= 2, -1.1= -2
	 * 5, ROUND_HALF_UP      四舍五入												例如：2.5= 3, -1.1= -1
	 * 6, ROUND_HALF_DOWN    五舍六入                             					例如：5.5= 5, -1.6= -2
	 * 7, ROUND_HALF_EVEN    
	 */
	
	public static final String BIG_NUM_FMT_COMMA = "#,###,###,###,###,###,##0.00";//千位分隔符
    public static final String BIG_NUM_FMT = "##################0.00";   //不带千位分隔符
    public static final int TWO_SCALE = 2;//保留2位小数
    public static final int FOUR_SCALE = 4;//保留4位小数
    public static final int SIX_SCALE = 6;//保留6位小数
    public static final String UNIT_TEN_THOUSAND = "万";
    public static final String UNIT_TEN_THOUSAND_CNY = "万元";
    public static final BigDecimal one = new BigDecimal("1");
    
    private ArithmeticUtil() {};
    
    /**
     * BigDecimal - 加法
     */
    public static BigDecimal add(String v1, String v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.add(b2);
    }

    /**
     * BigDecimal - 减法
     */
    public static BigDecimal sub(String v1,String v2){
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.subtract(b2);
    }

    /**
     * BigDecimal - 乘法
     */
    public static BigDecimal mul(String v1,String v2){
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.multiply(b2);
    }

    /**
     * BigDecimal - 除法
     */
    public static BigDecimal div(String v1, String v2, int scale, int roundingMode) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.divide(b2, scale, roundingMode);
    }

    /**
     * 保留小数位
     */
    public static BigDecimal round(String v, int scale, int roundingMode) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(v);
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, roundingMode);
    }
    
    /**
     * 保留小数位
     */
    public static BigDecimal round(BigDecimal b, int scale, int roundingMode) {
    		if(b == null) {
    			return BigDecimal.ZERO;
    		}
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, roundingMode);
    }

    /**
     * 分转换成元(2位小数点， 四舍五入)
     */
    public static BigDecimal penny2dollar(String v){
        BigDecimal s = div(v, "100", 2, BigDecimal.ROUND_HALF_UP);
        return s;
    }

    /**
     * 元转换成分
     */
    public static BigDecimal dollar2penny(String v){
        return mul(v, "100");
    }
    
    /**
     * 元转万元
     * 直接去除小数位
     * @param yuan
     * @param unit
     * @return
     */
    public static String yuan2TenThousand(BigDecimal yuan, String unit) {
//    		if(yuan == null) {
//    			return null;
//    		}
    		if(yuan == null || yuan.compareTo(BigDecimal.ZERO) <= 0) {
    			return StringUtil.addString("", "0", unit);
    		}
    		BigDecimal s = yuan.divide(new BigDecimal("10000"), 0, BigDecimal.ROUND_DOWN);
    		return StringUtil.addString("", s.toString(), unit);
    }

    /**
     * 格式化金额
     */
    public static String formatNumber(String v,String pattern) {
        return new DecimalFormat(pattern).format(new BigDecimal(v));
    }
    
    /** 
     * BigDecimal - 加法
     */  
   public static BigDecimal safeAdd(BigDecimal b1, BigDecimal... bn) {  
       if (null == b1) {  
           b1 = BigDecimal.ZERO;  
       }  
       if (null != bn) {  
           for (BigDecimal b : bn) {  
               b1 = b1.add(null == b ? BigDecimal.ZERO : b);  
           }  
       }  
       return b1;  
   }  
  
   /** 
    * BigDecimal - 减法
    * @param isZero    减法结果为负数时是否返回0，true是返回0（金额计算时使用），false是返回负数结果 
    * @param b1        被减数 
    * @param bn        需要减的减数数组 
    * @return 
    */  
   public static BigDecimal safeSubtract(Boolean isZero, BigDecimal b1, BigDecimal... bn) {  
       if (null == b1) {  
           b1 = BigDecimal.ZERO;  
       }  
       BigDecimal r = b1;  
       if (null != bn) {  
           for (BigDecimal b : bn) {  
               r = r.subtract((null == b ? BigDecimal.ZERO : b));  
           }  
       }  
       return isZero ? (r.compareTo(BigDecimal.ZERO) == -1 ? BigDecimal.ZERO : r) : r;  
   }  
  
  
   /** 
    * BigDecimal - 除法
    * 如果除数或者被除数为0，返回默认值 
    */  
   public static <T extends Number> BigDecimal safeDivide(T b1, T b2, BigDecimal defaultValue, int scale, int roundingMode) {  
       if (null == b1 || null == b2) {  
           return defaultValue;  
       }  
       try {  
           return BigDecimal.valueOf(b1.doubleValue()).divide(BigDecimal.valueOf(b2.doubleValue()), scale, roundingMode);  
       } catch (Exception e) {  
           return defaultValue;  
       }  
   }  
  
   /** 
    * BigDecimal - 乘法
    */  
   public static <T extends Number> BigDecimal safeMultiply(T b1, T b2, int scale, int roundingMode) {  
       if (null == b1 || null == b2) {  
           return BigDecimal.ZERO;  
       }  
       return BigDecimal.valueOf(b1.doubleValue()).multiply(BigDecimal.valueOf(b2.doubleValue())).setScale(scale, roundingMode);  
   }  
   
   /**
    * 获取金额占比(保留scale位小数点&四舍五入&*100)
    * @param total
    * @param part
    * @return
    */
   public static String getAmtPercentage(BigDecimal total, BigDecimal part) {
   		
   		if(total == null || part == null) {
   			return "0";
   		}
   		
   		BigDecimal percent = part.divide(total, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP);
   		if(percent.compareTo(new BigDecimal("100")) >0) {
   			return "100";
   		}
   		return percent.toString();
   }
   
   /**
    * 金额为空默认返回0
    * @param amt
    * @return
    */
   public static BigDecimal isEmptyReturnZero(BigDecimal amt) {
	   if(amt == null) {
		   return new BigDecimal("0");
	   }
	   return amt;
   }
   
   /**
    * 字符串转BigDecimal
    * @param str
    * @return
    */
   public static BigDecimal str2BigDecimal(String str) {
	   
	   if(StringUtil.isEmpty(str)) {
		   return null;
	   }
	   return new BigDecimal(str);
   }
   
   /**
    * BigDecimal转字符串
    * @param str
    * @return
    */
   public static String bigDecimal2Str(BigDecimal amt) {
	   
	   if(amt == null) {
		   return null;
	   }
	   return amt.toString();
   } 
   
    public static void main(String[] args) {
        System.out.println(ArithmeticUtil.add("1.99","20.999"));
        System.out.println(ArithmeticUtil.sub("1.99","20.999"));
        String s = "1111111.985";
        System.out.println(s);
    }

}
