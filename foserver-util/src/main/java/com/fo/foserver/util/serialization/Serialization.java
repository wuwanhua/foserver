package com.fo.foserver.util.serialization;

/**
 * java object serialization definition
 * @author wuqi
 */
public interface Serialization<T> {

	/**
	 * 具体的序列化实现
	 * @param object
	 * @return
	 */
	public T serialize(Object object);

	/**
	 * 获取反序列化的java对象
	 * @param serializedValue
	 * @return
	 */
	public Object deserialize(T serializedValue);

}
