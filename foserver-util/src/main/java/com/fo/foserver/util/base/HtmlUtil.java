package com.fo.foserver.util.base;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class HtmlUtil {
	
	private static final String REG_EX_SCRIPT = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
	private static final String REG_EX_STYLE = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
	private static final String REG_EX_HTML = "<[^>]+>"; // 定义HTML标签的正则表达式
	private static final String REG_EX_SPACE = "\\s*|\t|\r|\n";// 定义空格回车换行符

	/**
	 * 获得url最后一项
	 * 
	 * @see 如" http://192.168.0.1:8080/index.html/100100?a=a1&b=b2&c=c3"将截取
	 *      "100100"
	 * 
	 * @param url
	 *            url地址
	 * @return 地址栏最后一项
	 */
	public static String getLastPath(String url) {
		return url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("?"));
	}

	/**
	 * 根据参数名,获得指定url里的参数值
	 * 
	 * @see ?key=value&key1=value1
	 * @param url
	 *            url地址
	 * @param key
	 *            参数名
	 * @return 对应key的参数值
	 */
	public static String getParameterValue(String url, String key) {
		return getParameterMap(url).get(key);
	}

	/**
	 * 获取url里的参数
	 * 
	 * @param url
	 *            url地址
	 * @return String Json数据
	 */
	public static String getParameterJson(String url) {
		return JsonUtil.getJson(getParameterMap(url));
	}

	/**
	 * 获取url里的参数
	 * 
	 * @param url
	 *            url地址
	 * @return Map<String, String> map
	 */
	public static Map<String, String> getParameterMap(String url) {
		Map<String, String> queryStringMap = new HashMap<String, String>();
		if (!StringUtils.isEmpty(url)) {
			String queryString = url.substring(url.lastIndexOf("?") + 1);
			String[] queryStringSplit = queryString.split("&");
			for (String qs : queryStringSplit) {
				String[] queryStringParam = qs.split("=");
				queryStringMap.put(queryStringParam[0], queryStringParam[1]);
			}
		}
		return queryStringMap;
	}

	/**
	 * HTML字符转义
	 * 
	 * @see 对输入参数中的敏感字符进行过滤替换,防止用户利用JavaScript等方式输入恶意代码
	 * @see String input = <img src='http://t1.baidu.com/it/fm=0&gp=0.jpg'/>
	 * @see HtmlUtils.htmlEscape(input); //from spring.jar
	 * @see StringEscapeUtils.escapeHtml(input); //from commons-lang.jar
	 * @see 尽管Spring和Apache都提供了字符转义的方法,但Apache的StringEscapeUtils功能要更强大一些
	 * @see StringEscapeUtils提供了对HTML,Java,JavaScript,SQL,XML等字符的转义和反转义
	 * @see 但二者在转义HTML字符时,都不会对单引号和空格进行转义,而本方法则提供了对它们的转义
	 * @return String 过滤后的字符串
	 */
	public static String htmlEscape(String input) {
		if (StringUtils.isEmpty(input)) {
			return input;
		}
		input = input.replaceAll("&", "&amp;");
		input = input.replaceAll("<", "&lt;");
		input = input.replaceAll(">", "&gt;");
		input = input.replaceAll(" ", "&nbsp;");
		input = input.replaceAll("'", "&#39;"); // IE暂不支持单引号的实体名称,而支持单引号的实体编号,故单引号转义成实体编号,其它字符转义成实体名称
		input = input.replaceAll("\"", "&quot;"); // 双引号也需要转义，所以加一个斜线对其进行转义
		input = input.replaceAll("\n", "<br/>"); // 不能把\n的过滤放在前面，因为还要对<和>过滤，这样就会导致<br/>失效了
		return input;
	}

	/**
	 * 过滤script标签
	 * 
	 * @param htmlStr
	 *            需过滤的字符串
	 * @return 过滤后的字符串
	 */
	public static String delScriptTag(String htmlStr) {
		Pattern p_script = Pattern.compile(REG_EX_SCRIPT,
				Pattern.CASE_INSENSITIVE);
		Matcher m_script = p_script.matcher(htmlStr);
		htmlStr = m_script.replaceAll(""); // 过滤script标签
		return htmlStr.trim(); // 返回文本字符串
	}

	/**
	 * 过滤html标签
	 * 
	 * @param htmlStr
	 *            需过滤的字符串
	 * @return 过滤后的字符串
	 */
	public static String delHtmlTag(String htmlStr) {
		Pattern p_html = Pattern.compile(REG_EX_HTML, Pattern.CASE_INSENSITIVE);
		Matcher m_html = p_html.matcher(htmlStr);
		htmlStr = m_html.replaceAll(""); // 过滤html标签
		return htmlStr.trim(); // 返回文本字符串
	}

	/**
	 * 过滤style标签
	 * 
	 * @param htmlStr
	 *            需过滤的字符串
	 * @return 过滤后的字符串
	 */
	public static String delStyleTag(String htmlStr) {
		Pattern p_style = Pattern.compile(REG_EX_STYLE,
				Pattern.CASE_INSENSITIVE);
		Matcher m_style = p_style.matcher(htmlStr);
		htmlStr = m_style.replaceAll(""); // 过滤style标签
		return htmlStr.trim(); // 返回文本字符串
	}

	/**
	 * 过滤空格回车换行符
	 * 
	 * @param htmlStr
	 *            需过滤的字符串
	 * @return 过滤后的字符串
	 */
	public static String delSpaceAndEnterTag(String htmlStr) {
		Pattern p_space = Pattern.compile(REG_EX_SPACE,
				Pattern.CASE_INSENSITIVE);
		Matcher m_space = p_space.matcher(htmlStr);
		htmlStr = m_space.replaceAll(""); // 过滤空格回车标签
		return htmlStr.trim(); // 返回文本字符串
	}

	/**
	 * 
	 * 过滤所有Html标签和空格回车换行符
	 * 
	 * @param htmlStr
	 *            需过滤的字符串
	 * @return String 过滤后的字符串
	 * 
	 */
	public static String delAllTag(String htmlStr) {
		return delScriptTag(
				delHtmlTag(delStyleTag(delSpaceAndEnterTag(htmlStr)))).trim();
	}

	private HtmlUtil() {
		throw new AssertionError();
	}

}
