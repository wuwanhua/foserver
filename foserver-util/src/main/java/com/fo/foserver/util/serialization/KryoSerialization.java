package com.fo.foserver.util.serialization;

import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * kryo serialization implement
 *
 * @author wuqi
 */
public class KryoSerialization implements Serialization<byte[]> {

    private static final ThreadLocal<Kryo> kryoHolder = new ThreadLocal<Kryo>() {
        @Override
        protected Kryo initialValue() {
            Kryo kryo = new Kryo();
            kryo.setReferences(false);
            kryo.setRegistrationRequired(false);
            ((Kryo.DefaultInstantiatorStrategy) kryo.getInstantiatorStrategy())
                    .setFallbackInstantiatorStrategy(new StdInstantiatorStrategy());
            return kryo;
        }
    };

    @Override
    public byte[] serialize(Object object) {
        byte[] bytes = null;

        if (null == object) {
            return null;
        }
        Kryo kryo = kryoHolder.get();

        Output output = new Output(4096, 1024000);
        try {
            kryo.writeClassAndObject(output, object);
            bytes = output.toBytes();
            output.flush();
            output.clear();
        } finally {
            output.close();
        }
        return bytes;
    }

    @Override
    public Object deserialize(byte[] serializedValue) {
        if (null == serializedValue) {
            return null;
        }
        Kryo kryo = kryoHolder.get();
        Object obj = null;
        Input input = new Input(serializedValue);
        try {
            obj = kryo.readClassAndObject(input);
        } finally {
            input.close();
        }
        return obj;
    }

}
