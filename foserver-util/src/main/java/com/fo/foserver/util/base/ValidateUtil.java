package com.fo.foserver.util.base;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证类工具类
 */
public class ValidateUtil {
	
	/**
	 * 性别验证
	 * 用户性别  1-男  2-女
	 * @param gender
	 * @return
	 */
	public static boolean isGender(Integer gender) {
		
		if(gender != null && (gender == 1 || gender == 2)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 邮箱地址验证
	 * 
	 * 合法E-mail地址：   
	 *   1. 必须包含一个并且只有一个符号“@”   
	 *   2. 第一个字符不得是“@”或者“.”   
	 *   3. 不允许出现“@.”或者.@   
	 *   4. 结尾不得是字符“@”或者“.”   
	 *   5. 允许“@”前的字符中出现“＋”   
	 *   6. 不允许“＋”在最前面，或者“＋@”
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		String check = "^(\\w+((-\\w+)|(\\.\\w+))*)\\+\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$";
		Pattern regex = Pattern.compile(check);  
		Matcher matcher = regex.matcher(email);  
		return matcher.matches();
	}
	
	/**
	 * 判断用户昵称是否合法  
	 * 昵称长度暂定必须小于30
	 * @param nickname
	 * @return
	 */
	public static boolean isNickname(String nickname) {
		
		if(!StringUtil.isEmpty(nickname) && nickname.length() <20) {
			return true;
		}
		return false;
	}
	
	/**
	 * 判断是否是固定电话 11或者 12位
	 * @param phone
	 * @return
	 */
	public static boolean isPhone(String phone){
		if(!StringUtil.isEmpty(phone) && isNumber(phone)){
			if(phone.length() == 11 || phone.length() == 12){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 验证字符串是否全都是数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str){
		
		Pattern pattern = Pattern.compile("[0-9]*");  
		return pattern.matcher(str).matches();
	}
	
	/**
	 * 验证手机号
	 * @param mobile
	 * @return
	 */
	public static boolean isMobile(String mobile) {
		
		Pattern p = Pattern.compile("^1[3|4|5|7|8][0-9]\\d{8}$");
		Matcher m = p.matcher(mobile);
		return m.matches();
	}
	
	/**
	 * 身份证号验证
	 * 
	 * @param idCard
	 * @return
	 */
	public static boolean isIdCard(String idCard){
		
		return IdCardUtil.validateCard(idCard);
	}
	
	/**
	 * 验证密码
	 * @param password
	 * @return
	 */
	public static boolean isPassword(String password) {
		
		Pattern p = Pattern.compile("^([0-9a-zA-Z\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\-\\_]){6,16}$");
		Matcher m = p.matcher(password);
		return m.matches();
	}
	
	/**
	 * 手势密码验证
	 * @param password
	 * @return
	 */
	public static boolean isGesturePassword(String password) {
		Pattern p = Pattern.compile("^([0-9]){4,16}$");
		Matcher m = p.matcher(password);
		return m.matches();
	}
	
	/**
     * 检查汉字（2~4个汉字认为是合法的）
     *
     * @param value
     * @return
     */
    public static boolean isRealName(String value) {
        String rex = "^[\u4e00-\u9fa5]{2,10}$";
        Pattern pattern = Pattern.compile(rex);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
    
    /**
     * 募集账号长度限制
     * @param bankNumber
     * @return
     */
    public static boolean isRaiseBankNumber(String bankNumber) {
    		if(StringUtil.isEmpty(bankNumber) || bankNumber.length() > 30) {
    			return false;
    		}
    		return true;
    }
    
    /**
     * 检查日期格式 2016-06-14 (yyyy-MM-dd)
     * @param str
     * @return
     */
    public static boolean isValidDate(String str) {  
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
        try{  
            Date date = (Date)formatter.parse(str);  
            return str.equals(formatter.format(date));  
        }catch(Exception e){  
            return false;  
        }  
    } 
    
}

