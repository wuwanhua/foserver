package com.fo.foserver.util.base;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);
	public static final int INTERVAL_DAY = 1;
	public static final int INTERVAL_WEEK = 2;
	public static final int INTERVAL_MONTH = 3;
	public static final int INTERVAL_YEAR = 4;
	public static final int INTERVAL_HOUR = 5;
	public static final int INTERVAL_MINUTE = 6;
	public static final int INTERVAL_SECOND = 7;
	public static final Date tempDate = new Date(new Long("-2177481952000").longValue());
	public static final String YYYY_MM = "yyyy-MM";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";
	public static final String YEAR_YYYY_MM_DD = "yyyy年MM月dd日";
	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String YYYYMMDD = "yyyyMMdd";
	public static final String YYYY = "yyyy";

	/** 
	 * 得到指定月的天数 
	 * */
	public static int getMonthLastDay(int year, int month) {
		Calendar a = Calendar.getInstance();
		a.set(Calendar.YEAR, year);
		a.set(Calendar.MONTH, month - 1);
		a.set(Calendar.DATE, 1);//把日期设置为当月第一天  
		a.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天  
		int maxDate = a.get(Calendar.DATE);
		return maxDate;
	}

	public static Date dateFormatSupp(String date) {

		if (date == null) {
			return null;
		}
		String nowYMD = dateFormat(new Date()).substring(0, 10);
		return dateFormat(nowYMD + " " + date.trim());
	}

	public static boolean isToday(Date date) {
		Calendar in = Calendar.getInstance();
		in.setTime(date);
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		boolean result = true;
		result &= now.get(Calendar.YEAR) == in.get(Calendar.YEAR);
		result &= now.get(Calendar.MONTH) == in.get(Calendar.MONTH);
		result &= now.get(Calendar.DATE) == in.get(Calendar.DATE);
		return result;
	}

	public static long DaysBetween(Date date1, Date date2) {
		if (date2 == null)
			date2 = new Date();
		long day = (date2.getTime() - date1.getTime()) / 86400000L;
		return day;
	}

	public static boolean compareDate(String date1, String date2) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date d1 = format.parse(date1);
			Date d2 = format.parse(date2);
			return !d1.after(d2);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	public static Date dateFormat(String date, String dateFormat) {
		if (date == null)
			return null;
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		if (date != null)
			try {
				return format.parse(date);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		return null;
	}

	public static Date dateFormat(String date) {
		return dateFormat(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static Date dateFormatOnlyDate(String date) {
		return dateFormat(date, "yyyy-MM-dd");
	}

	public static String formatDate(Date date) {
		if (date == null)
			return "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	public static String formatDate(Date date, String dateFormat) {
		if (date == null)
			return "";
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(date);
	}

	public static String formatDateSeconds(Date date) {
		if (date == null)
			return "";
		SimpleDateFormat format = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
		return format.format(date);
	}

	public static Date formatDateToDate(Date date) {
		String da = formatDateSeconds(date);
		if (StringUtil.isEmpty(da)) {
			return null;
		}
		return dateFormat(da);
	}

	public static String dateFormat(Date date, String dateFormat) {
		if (date == null)
			return "";
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(date);
	}

	public static String birthdayFormat(Date date) {
		if (date == null)
			return "";
		SimpleDateFormat format = null;
		if (date.before(tempDate))
			format = new SimpleDateFormat("MM-dd");
		else {
			format = new SimpleDateFormat("yyyy-MM-dd");
		}
		return format.format(date);
	}

	public static String dateFormat(Date date) {
		return dateFormat(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static String dateFormatOther(Date date) {
		return dateFormat(date, "yyyy.MM.dd HH:mm:ss");
	}

	public static String dateFormatOnlyDate(Date date) {
		return dateFormat(date, "yyyy-MM-dd");
	}

	public static boolean isExpiredDay(Date date1) {
		long day = (new Date().getTime() - date1.getTime()) / 86400000L;

		return day >= 1L;
	}

	public static Date getYesterday() {
		Date date = new Date();
		long time = date.getTime() / 1000L - 86400L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	public static Date getWeekAgo() {
		Date date = new Date();
		long time = date.getTime() / 1000L - 604800L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	public static String getDaysAgo(int interval) {
		Date date = new Date();
		long time = date.getTime() / 1000L - interval * 60 * 60 * 24;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.format(date);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return "";
	}

	public static Date getTomorrow() {
		Date date = new Date();
		long time = date.getTime() / 1000L + 86400L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	/**
	 * 获得第二天时间
	 */
	public static String getTomorrowDate() {
		Date date = new Date();
		long time = date.getTime() / 1000L + 86400L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			String tomarrow = format.format(date);
			return tomarrow;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return null;
	}

	public static Date getBeforeDate(String range) {
		Calendar today = Calendar.getInstance();
		if ("week".equalsIgnoreCase(range))
			today.add(4, -1);
		else if ("month".equalsIgnoreCase(range))
			today.add(2, -1);
		else
			today.clear();
		return today.getTime();
	}

	public static Date getThisWeekStartTime() {
		Calendar today = Calendar.getInstance();
		today.set(7, today.getFirstDayOfWeek());
		Calendar weekFirstDay = Calendar.getInstance();
		weekFirstDay.clear();
		weekFirstDay.set(1, today.get(1));
		weekFirstDay.set(2, today.get(2));
		weekFirstDay.set(5, today.get(5));
		return weekFirstDay.getTime();
	}

	public static String getToday() {

		return getToday("yyyy-MM-dd");
	}

	public static String getToday(String format) {
		String result = "";
		try {
			Date today = new Date();
			SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
			result = simpleFormat.format(today);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	/**
	 * 获取今天
	 * @return
	 */
	public static Date getTodayDate() {
		Date date = new Date();
		//		long time = date.getTime() / 1000L + 86400L;
		//		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	public static Date getStartDay(int year, int month) {
		Calendar today = Calendar.getInstance();
		today.clear();
		today.set(1, year);
		today.set(2, month - 1);
		today.set(5, 1);
		return today.getTime();
	}

	public static Date getMonthEndDay(int year, int month) {
		Calendar today = Calendar.getInstance();
		today.clear();
		today.set(Calendar.YEAR, year);
		today.set(Calendar.MONTH, month);
		today.set(Calendar.DATE, 0);//reduce 1
		return today.getTime();
	}

	public static List<Integer> getBeforeYearList(int before) {
		Calendar today = Calendar.getInstance();
		int theYear = today.get(1);
		List<Integer> list = new ArrayList<Integer>();
		for (int i = before; i >= 0; i--) {
			list.add(Integer.valueOf(theYear - i));
		}
		return list;
	}

	public static Date dateAdd(int interval, Date date, int n) {
		long time = date.getTime() / 1000L;
		switch (interval) {
		case 1:
			time += n * 86400;
			break;
		case 2:
			time += n * 604800;
			break;
		case 3:
			time += n * 2678400;
			break;
		case 4:
			time += n * 31536000;
			break;
		case 5:
			time += n * 3600;
			break;
		case 6:
			time += n * 60;
			break;
		case 7:
			time += n;
			break;
		}

		Date result = new Date();
		result.setTime(time * 1000L);
		return result;
	}

	public static int dateDiff(int interval, Date begin, Date end) {
		long beginTime = begin.getTime() / 1000L;
		long endTime = end.getTime() / 1000L;
		long tmp = 0L;
		if (endTime == beginTime) {
			return 0;
		}

		if (endTime < beginTime) {
			tmp = beginTime;
			beginTime = endTime;
			endTime = tmp;
		}

		long intervalTime = endTime - beginTime;
		long result = 0L;
		switch (interval) {
		case 1:
			result = intervalTime / 86400L;
			break;
		case 2:
			result = intervalTime / 604800L;
			break;
		case 3:
			result = intervalTime / 2678400L;
			break;
		case 4:
			result = intervalTime / 31536000L;
			break;
		case 5:
			result = intervalTime / 3600L;
			break;
		case 6:
			result = intervalTime / 60L;
			break;
		case 7:
			result = intervalTime / 1L;
			break;
		}

		if (tmp > 0L) {
			result = 0L - result;
		}
		return (int) result;
	}

	public static int getTodayYear() {
		int yyyy = Integer.parseInt(dateFormat(new Date(), "yyyy"));
		return yyyy;
	}

	public static Date getNow() {
		return new Date();
	}

	public static String dateFormatRss(Date date) {
		if (date != null) {
			return dateFormat(date, "E, d MMM yyyy H:mm:ss") + " GMT";
		}
		return "";
	}

	public static boolean betweenStartDateAndEndDate(Date startDate, Date endDate) {
		boolean bool = false;
		Date curDate = new Date();
		if ((curDate.after(startDate)) && (curDate.before(dateAdd(1, endDate, 1)))) {
			bool = true;
		}
		return bool;
	}

	/**
	 * 比较某个时间是否在另外两个时间内
	 * @param currentDate
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static boolean curBetweenStartDateAndEndDate(Date currentDay, Date startDate, Date endDate) {
		boolean bool = false;
		Date curDate = currentDay;
		if ((curDate.after(startDate)) && (curDate.before(endDate))) {
			bool = true;
		}
		return bool;
	}

	public static boolean nowDateBetweenStartDateAndEndDate(Date startDate, Date endDate) {
		boolean bool = false;
		Date curDate = new Date();
		if ((curDate.after(startDate)) && (curDate.before(endDate))) {
			bool = true;
		}
		return bool;
	}

	public static boolean nowDateAfterDate(Date date) {
		boolean bool = false;
		Date curDate = new Date();
		if (curDate.after(date)) {
			bool = true;
		}
		return bool;
	}

	public static int getBetweenTodaysStartDateAndEndDate(Date startDate, Date endDate) {
		int betweentoday = 0;
		if (startDate == null) {
			return betweentoday;
		}
		if (endDate == null) {
			Calendar calendar = Calendar.getInstance();
			String year = new Integer(calendar.get(1)).toString();
			String month = new Integer(calendar.get(2) + 1).toString();
			String day = new Integer(calendar.get(5)).toString();
			String strtodaytime = year + "-" + month + "-" + day;
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				endDate = formatter.parse(strtodaytime);
			} catch (ParseException e) {
				logger.error(e.getMessage(), e);
			}
		}

		if (endDate.after(startDate))
			betweentoday = (int) ((endDate.getTime() - startDate.getTime()) / 86400000L);
		else {
			betweentoday = (int) ((startDate.getTime() - endDate.getTime()) / 86400000L);
		}
		return betweentoday;
	}

	public static String getTime(int format) {
		StringBuffer cTime = new StringBuffer(10);
		Calendar time = Calendar.getInstance();
		int miltime = time.get(14);
		int second = time.get(13);
		int minute = time.get(12);
		int hour = time.get(11);
		int day = time.get(5);
		int month = time.get(2) + 1;
		int year = time.get(1);
		if (format != 14) {
			if (year >= 2000)
				year -= 2000;
			else
				year -= 1900;
		}
		if (format >= 2) {
			if (format == 14)
				cTime.append(year);
			else
				cTime.append(getFormatTime(year, 2));
		}
		if (format >= 4)
			cTime.append(getFormatTime(month, 2));
		if (format >= 6)
			cTime.append(getFormatTime(day, 2));
		if (format >= 8)
			cTime.append(getFormatTime(hour, 2));
		if (format >= 10)
			cTime.append(getFormatTime(minute, 2));
		if (format >= 12)
			cTime.append(getFormatTime(second, 2));
		if (format >= 15)
			cTime.append(getFormatTime(miltime, 3));
		return cTime.toString();
	}

	private static String getFormatTime(int time, int format) {
		StringBuffer numm = new StringBuffer();
		int length = String.valueOf(time).length();
		if (format < length)
			return null;
		for (int i = 0; i < format - length; i++) {
			numm.append("0");
		}
		numm.append(time);
		return numm.toString().trim();
	}

	public static int getUserAge(Date birthday) {
		if (birthday == null)
			return 0;
		Calendar cal = Calendar.getInstance();
		if (cal.before(birthday)) {
			return 0;
		}
		int yearNow = cal.get(1);
		cal.setTime(birthday);
		int yearBirth = cal.get(1);
		return yearNow - yearBirth;
	}

	public static Date getDateByUnixTime(int unixTime) {
		return new Date(unixTime * 1000L);
	}

	public static int getUnixTimeByDate(Date date) {
		return (int) (date.getTime() / 1000L);
	}

	public static Date getNextDay(Date date) {
		long time = date.getTime() / 1000L + 86400L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	public static Date nextDay(Date date) {
		Date newDate = (Date) date.clone();
		long time = newDate.getTime() / 1000L + 86400L;
		newDate.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			newDate = format.parse(format.format(newDate));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return newDate;
	}

	public static Date getNowTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String dateStr = dateFormat(date);
		try {
			date = format.parse(dateStr);
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		return date;
	}

	public static Date getTomorrow(Date date1) {
		Calendar now = Calendar.getInstance();
		now.setTime(date1);

		now.add(5, 1);
		return now.getTime();
	}

	public static Date getWeekAgo(Date date) {
		Date newDate = (Date) date.clone();
		long time = newDate.getTime() / 1000L - 604800L;
		newDate.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			newDate = format.parse(format.format(newDate));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return newDate;
	}

	public static Date getDatebyTime(Date date, int n) {
		String str = dateFormat(date, "yyyy-MM-dd");
		String[] strs = str.split("-");
		int month = Integer.parseInt(strs[1]);
		int monthnow = (month + n) % 12;
		int year = Integer.parseInt(strs[0]) + (month + n) / 12;
		str = String.valueOf(year) + "-" + String.valueOf(monthnow) + "-" + strs[2];

		return dateFormat(str, "yyyy-MM-dd");
	}

	public static Date yesterday(Date date) {
		Date newDate = (Date) date.clone();
		long time = newDate.getTime() / 1000L - 86400L;
		newDate.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			newDate = format.parse(format.format(newDate));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return newDate;
	}

	public static Date getYesterday(Date date) {
		long time = date.getTime() / 1000L - 86400L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	public static String getStringNowTime() {
		Date date = new Date();
		String dateStr = dateFormat(date);

		return dateStr;
	}

	public static long getSpecifyTimeSec(long time, int range) {
		Date date = new Date((time * 1000L + 28800000L) / 86400000L * 86400000L - 28800000L);
		long zeroTime = date.getTime() / 1000L;
		long specifyTime = range * 24 * 3600;
		return zeroTime + specifyTime;
	}

	public static String formatDateByUnixTime(long unixTime, String dateFormat) {
		return dateFormat(new Date(unixTime * 1000L), dateFormat);
	}

	public static long getSecondsFromHour(int hour) {
		long s = 0;
		if (hour > 0) {
			s = hour * 60 * 60;
		}
		return s;
	}

	/**
	 * 将类似2013-10-22 10:15:32转化为1375931010000毫秒型
	 * 
	 * @param yyyy
	 * @param regex eg:yyyy-MM-dd hh:mm:ss
	 * @return
	 */
	public static Date getDateFromDateStr(String dataStr, String regex) {
		SimpleDateFormat formater = new SimpleDateFormat(regex);
		try {
			Date date = formater.parse(dataStr);
			return date;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * @param date 日期
	 * @return 得到这天开始的时间
	 */
	public static Date getBeginDateTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.AM_PM, 0);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}

	/**
	 * @param date 日期
	 * @return 得到这天最后结束的时间
	 */
	public static Date getEndDateTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getBeginDateTime(date));
		calendar.set(Calendar.AM_PM, 0);
		calendar.add(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	public static void main(String[] args) {
		//		System.out.println(getDaysAgo(3));//end time
		//		
		//		System.out.println(getDaysAgo(7));

		//		System.out.println(getCalendarField(11,dateFormat("2017-05-22 23:59:59", YYYY_MM_DD_HH_MM_SS)));
		int ss = DateUtil.dateDiff(DateUtil.INTERVAL_HOUR, DateUtil.dateFormat("2017-06-06 00:00:00", YYYY_MM_DD_HH_MM_SS),
				DateUtil.dateFormat("2017-06-07 00:00:59", YYYY_MM_DD_HH_MM_SS));
		System.out.println(ss);

	}

	/**
	 * 根据日期获取时间，为空则返回null
	 * 
	 * @param date
	 * @return
	 */
	public static String getMillisecond(Date date) {
		if (date == null) {
			return null;
		}
		return String.valueOf(date.getTime());
	}

	public static String getSecond(Date date) {
		if (date == null) {
			return null;
		}
		return String.valueOf((int) (date.getTime() / 1000));
	}

	public static boolean areSameDay(Date dateA, Date dateB) {
		if (null == dateA || null == dateB) {
			return false;
		}
		Calendar calDateA = Calendar.getInstance();
		calDateA.setTime(dateA);
		Calendar calDateB = Calendar.getInstance();
		calDateB.setTime(dateB);
		return calDateA.get(Calendar.YEAR) == calDateB.get(Calendar.YEAR)
				&& calDateA.get(Calendar.MONTH) == calDateB.get(Calendar.MONTH)
				&& calDateA.get(Calendar.DAY_OF_MONTH) == calDateB.get(Calendar.DAY_OF_MONTH);
	}

	public static final int getMinute(Date date) {
		return getCalendarField(Calendar.MINUTE, date);
	}

	/**
	 * 获取时间某个时间元素的值 field参数的值以java.util.Calendar为准 DAY_OF_MONTH = 5; DAY_OF_YEAR
	 * = 6; DAY_OF_WEEK = 7; DAY_OF_WEEK_IN_MONTH = 8; AM_PM = 9; HOUR = 10;
	 * HOUR_OF_DAY = 11; MINUTE = 12; SECOND = 13;
	 * 
	 * @param field
	 * @param date
	 * @return
	 */
	public static int getCalendarField(int field, Date date) {
		if (null == date) {
			return 0;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(field);
	}

	/**
	 * 获取当前 秒 级别的时间戳
	 * @return
	 * 
	 * @author mingwang
	 */
	public static Long getNowTimeStamp() {
		return Long.valueOf(getUnixTimeByDate(getNowTime()));
	}

	public static Long getTimeStamp(Date date) {
		if (date == null) {
			return null;
		}

		return date.getTime();
	}

	/**
	 * 一周后的日期
	 * @return
	 */
	public static Date getWeekAfter() {
		Date date = new Date();
		long time = date.getTime() / 1000L + 604800L;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = format.parse(format.format(date));
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	/**
	 * 指定日子后的日期
	 * @param interval
	 * @return
	 */
	public static String getDaysAfter(int interval) {
		Date date = new Date();
		long time = date.getTime() / 1000L + interval * 60 * 60 * 24;
		date.setTime(time * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.format(date);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return "";
	}

	/**
	 * 日期加减
	 * @param endTime
	 * @param num
	 * @return
	 */
	public static Date dateAdd(Date endTime, int num, int calendarType) {

		if (endTime == null) {
			return null;
		}

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(endTime);
		calendar.add(calendarType, num);

		return calendar.getTime();
	}

	/**
	 * @Title: formatDateByTime
	 * @Description: 时间戳转时间
	 * @param @param times
	 * @param @return   
	 * @return String    
	 * @throws
	 */
	public static String formatDateByTime(Long time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(time);
	}

	public static boolean betweenStartDateAndEndDate(Date currentDate, Date startDate, Date endDate) {
		boolean bool = false;
		Date curDate = currentDate;
		if ((curDate.after(startDate)) && (curDate.before(DateUtil.dateAdd(1, endDate, 1)))) {
			bool = true;
		}
		return bool;
	}

	/**
	 * @Title: totalSecStartAndEndDate
	 * @Description: 获取开始时间到结束时间之间的秒数
	 * @param @param startDate
	 * @param @param endDate
	 * @param @return   
	 * @return Long    
	 * @throws
	 */
	public static Long getSecStartAndEndDate(Date startDate, Date endDate) {

		if (startDate == null || endDate == null || startDate.compareTo(endDate) > 0) {
			return null;
		}

		return (endDate.getTime() - startDate.getTime()) / 1000;
	}

	/**
	 * 获取当前年份第一次风险评估次数
	 */
	public static Integer getFirstAuthTimesCurrentYear(Date now) {

		//获取当前年份第一次风险评估次数  如: 201701
		String firstTimeValue = StringUtil.addString("", dateFormat(now, YYYY), "01");
		if (StringUtil.isEmpty(firstTimeValue) || firstTimeValue.length() != 6) {
			return null;
		}

		//检查数据正确性
		Integer firstTime = StringUtil.toIntValue(firstTimeValue);
		Integer initTime = getInitAuthTimesCurrentYear(now);
		if (firstTime == null || firstTime == 0 || initTime == null || initTime == 0 || (firstTime - initTime) < 1) {
			return null;
		}

		return firstTime;
	}

	/**
	 * 获取当年年份风险评估第0次数据， 用于计算当前年份的总评估次数
	 */
	public static Integer getInitAuthTimesCurrentYear(Date now) {

		//获取初始化数据   如： 201700
		String initValue = StringUtil.addString("", dateFormat(now, YYYY), "00");
		if (StringUtil.isEmpty(initValue) || initValue.length() != 6) {
			return null;
		}

		//检查数据正确性
		Integer init = StringUtil.toIntValue(initValue);
		if (init == null || init == 0) {
			return null;
		}

		return init;
	}

	/**
	 * 根据评估次数获取年份第0次评估次数
	 * @param authTiems
	 * @return
	 */
	public static Integer getAuthTimesInitValue(Integer authTiems) {

		if (authTiems == null) {
			return null;
		}

		String authTiemsStr = authTiems.toString();
		if (StringUtil.isEmpty(authTiemsStr) || authTiemsStr.length() != 6) {
			return null;
		}

		String init = StringUtil.addString("", authTiemsStr.substring(0, 4), "00");
		Integer initValue = StringUtil.toIntValue(init);
		if (initValue == null || initValue == 0) {
			return null;
		}

		return initValue;
	}

	/**
	 * 根据总次数和已评估次数获取剩余次数
	 * @param totalTimes
	 * @param authTimes
	 * @return
	 */
	public static Integer getRemainAuthTimes(Integer totalTimes, Integer authTimes) {

		if (totalTimes == null) {
			return 0;
		}

		if (StringUtil.isEmptyInteger(authTimes)) {
			return totalTimes;
		}

		Integer remain = totalTimes - StringUtil.toIntValue(authTimes.toString().substring(4)); //201702 -> 02
		if (remain < 0) {
			remain = 0;
		}
		return remain;
	}

}
