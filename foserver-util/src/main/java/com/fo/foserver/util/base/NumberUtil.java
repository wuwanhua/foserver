package com.fo.foserver.util.base;

import java.util.Random;

public class NumberUtil {
	
	public static void main(String[] args) {
		
		
		System.out.println(distance(106.59009, 29.581496, 106.59009, 29.583506));
	}
	
	/**
	 * 极端经纬度之间的距离
	 * @param longitude 打卡经度
	 * @param latitude 打卡纬度
	 * @param storeLongitude 店铺经度
	 * @param storeLatitude 店铺纬度
	 * @return
	 */
	public static double distance(double longitude, double latitude, double storeLongitude, double storeLatitude){
		
		double a, b, R;  
	    R = 6378137; // 地球半径  
	    latitude = latitude * Math.PI / 180.0;  
	    storeLatitude = storeLatitude * Math.PI / 180.0;  
	    a = latitude - storeLatitude;  
	    b = (longitude - storeLongitude) * Math.PI / 180.0;  
	    double d;  
	    double sa2, sb2;  
	    sa2 = Math.sin(a / 2.0);  
	    sb2 = Math.sin(b / 2.0);  
	    d = 2  
	            * R  
	            * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(latitude)  
	                    * Math.cos(storeLatitude) * sb2 * sb2));  
	    return d;  
	}
	
	/**
	 * 随机数
	 * @param max
	 * @param min
	 * @return
	 */
	public static long rand(int max, int min) {
		return Math.round(Math.random()*(max-min)+min);
	}
	
	/**
	 * @Title: uniqueId
	 * @Description: 13bit(时间戳) + num bit(随机数)
	 * @param @param num 随机数位数
	 * @param @return   
	 * @return Long    
	 * @throws
	 */
	public static Long uniqueId(long time, int num){
		
		String date = Long.valueOf(time).toString();
		String rand = "";
		if(num > 0){
			rand = getRandNum(rand(getNum(num), 0), num);
		}
		return Long.valueOf(StringUtil.addString("", date, rand));
	}
	

	public static String getRandNum(long rand, int num) {
		
		if(num < 1){
			return "";
		}
		
		StringBuilder temp = new StringBuilder();
		for(int i=0; i<num; i++){
			temp.append("0");
		}
		
		String ran = Long.valueOf(rand).toString();
		if(StringUtil.isEmpty(ran)){
			return "";
		}
		
		if(ran.length()> temp.length()){
			return ran.substring(ran.length()-num);
		}else{
			return StringUtil.addString("", temp.substring(0, temp.length()-ran.length()), ran);
		}
	}

	/**
	 * @Title: logId
	 * @Description: 获取日志组唯一编号
	 * @param @return   
	 * @return Long    
	 * @throws
	 */
	public static Long logId(){
		long time = System.currentTimeMillis() - 1484637886385L;
		return uniqueId(time, 4);
	}
	
	/**
	 * @Title: getNum
	 * @Description: 根据num获取最大值        num=1返回9   num=2返回99
	 * @param @param num
	 * @param @return   
	 * @return int    
	 * @throws
	 */
	private static int getNum(int num) {
		
		if(num > 0){
			StringBuilder str = new StringBuilder();
			for(int i=0; i<num; i++){
				str.append("9");
			}
			
			if(StringUtil.isEmpty(str.toString())){
				return 0;
			}
			
			return Integer.valueOf(str.toString());
		}else{
			return 0;
		}
	}

	/**
	 * double保留n位小数
	 * 
	 * @param d
	 *            目标double
	 * @param n
	 *            保留n位小数
	 * @return 返回保留n位小数的double
	 */
	public static double retainDecimal(double d, int n) {
		double p = Math.pow(10, n);
		return Math.round(d * p) / p;
	}

	/**
	 * 交换两个数
	 * 
	 * @see 请将要交换的两个数放在数组的0和1号位
	 * 
	 * @param a
	 *            交换前的数组
	 * @return 交换后的数组
	 */
	public static Integer[] exchange(Integer[] a) {
		a[0] ^= a[1];
		a[1] ^= a[0];
		a[0] ^= a[1];
		return a;
	}

	/**
	 * 判断字符串是否是整数int
	 * 
	 * @param value
	 *            字符串
	 * @return true or false
	 */
	public static boolean isInteger(String value) {
		return value != null ? value.matches("^[-]{0,1}[0]*[0-9]{1,10}$")
				: false;
	}

	/**
	 * 判断字符串是否是长整数long
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isLong(String value) {
		return (!StringUtil.isEmpty(value) && value.length() < 19) ? value
				.matches("^[-]{0,1}[0]*[0-9]*$") : false;
	}

	/**
	 * 判断字符串是否是小数
	 * 
	 * @param value
	 *            字符串
	 * @return true or false
	 */
	public static boolean isDouble(String value) {
		return (!StringUtil.isEmpty(value) && value.length() < 11) ? value
				.matches("^[-]{0,1}[0-9]*[.][0-9]{1,}$") : false;
	}

	/**
	 * 判断String是否是数字
	 * 
	 * @param value
	 *            字符串
	 * @return true or false
	 */
	public static boolean isNumber(String value) {
		return isInteger(value) || isDouble(value);
	}

	/**
	 * 是否是质数
	 * 
	 * @param num
	 *            整数
	 * @return true or false
	 */
	public static boolean isPrimes(int num) {
		for (int i = 2; i <= Math.sqrt(num); i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 生成不重复随机整数
	 * 
	 * @see 根据给定的最小数字和最大数字，以及随机数的个数，产生指定的不重复的数组
	 * 
	 * @param begin
	 *            最小数字（包含该数）
	 * @param end
	 *            最大数字（不包含该数）
	 * @param size
	 *            指定产生随机数的个数
	 * @return 随机产生的整数数组
	 */
	public static int[] generateRandomNumber(int begin, int end, int size) {
		// 加入逻辑判断，确保begin<end并且size不能大于该表示范围
		if (begin >= end || (end - begin) < size) {
			return null;
		}
		// 种子可以随意生成，但不能重复
		int[] seed = new int[end - begin];

		for (int i = begin; i < end; i++) {
			seed[i - begin] = i;
		}
		int[] ranArr = new int[size];
		Random ran = new Random();
		// 数量你可以自己定义。
		for (int i = 0; i < size; i++) {
			// 得到一个位置
			int j = ran.nextInt(seed.length - i);
			// 得到那个位置的数值
			ranArr[i] = seed[j];
			// 将最后一个未用的数字放到这里
			seed[j] = seed[seed.length - 1 - i];
		}
		return ranArr;
	}

	/**
	 * N的阶乘
	 * 
	 * @see n*(n-1)*(n-2)......
	 * 
	 * @param n
	 *            整数
	 * @return 阶乘
	 */
	public static int factorial(int n) {
		return n < 1 ? 0 : n == 1 ? 1 : n * factorial(n - 1);
	}

	/**
	 * 平方根算法
	 * 
	 * @see 整数
	 * 
	 * @param x
	 *            准备开方的整数
	 * @return 平方根
	 */
	public static long sqrt4Long(long x) {
		long y = 0;
		long b = (~Long.MAX_VALUE) >>> 1;
		while (b > 0) {
			if (x >= y + b) {
				x -= y + b;
				y >>= 1;
				y += b;
			} else {
				y >>= 1;
			}
			b >>= 2;
		}
		return y;
	}

	/**
	 * 平方根算法
	 * 
	 * @see 小数
	 * @param x
	 *            准备开方的小数
	 * @return 平方根
	 */
	public static double sqrt(double x) {
		return x < 0 ? 0 : Math.sqrt(x);
	}

	/**
	 * 求m和n的最大公约数
	 * 
	 * @param m
	 *            第一个整数
	 * @param n
	 *            第二个整数
	 * @return m和n的最大公约数
	 */
	public static int greatestCommonDivisor(int m, int n) {
		while (m % n != 0) {
			int temp = m % n;
			m = n;
			n = temp;
		}
		return n;
	}

	/**
	 * 求m和n的最小公倍数
	 * 
	 * @param m
	 *            第一个整数
	 * @param n
	 *            第二个整数
	 * @return m和n的最小公倍数
	 */
	public static int leastCommonMultiple(int m, int n) {
		return m * n / greatestCommonDivisor(m, n);
	}

}
