package com.fo.foserver.util.http;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLHandshakeException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.util.base.ObjectUtil;
import com.fo.foserver.util.base.StringUtil;
import com.fo.foserver.util.result.MsgException;
import com.fo.foserver.util.result.Result;

import net.sf.json.JSONObject;

public class HttpClientService {

	private static final Logger logger = LoggerFactory.getLogger(HttpClientService.class);

	/**
	 * user_agent
	 */
	private static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";

	/**
	 * 最大连接数
	 */
	private int maxConnections = 5000;

	/**
	 * 链接超时时间
	 */
	private int connectionTimeout = 3000;

	/**
	 * 每个主机最大连接数
	 */
	private int maxPerRoute = 100;

	/**
	 * 读取超时时间
	 */
	private int readTimeout = 5000;

	/**
	 * 请求失败后重试次数
	 */
	private int retry = 0;

	private String requestEncoding = "utf-8";

	private String responseEncoding = "utf-8";

	private int httpCode = 0;

	private Map<String, String> head;

	/**
	 * httpClient
	 */
	private CloseableHttpClient httpClient;

	public HttpClientService init() {
		// synchronized (this) {
		if (this.httpClient == null) {
			this.creatHttpClient();
		}
		// }
		return this;
	}

	public void creatHttpClient() {
		PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
		manager.setMaxTotal(maxConnections);
		manager.setDefaultMaxPerRoute(maxPerRoute);

		RequestConfig config = RequestConfig.custom().setSocketTimeout(readTimeout).setConnectTimeout(connectionTimeout)
				.build();// 设置请求和传输超时时间

		httpClient = HttpClients.custom().setConnectionManager(manager).setDefaultRequestConfig(config)
				.setRetryHandler(new HttpRequestRetryHandler() {
					@Override
					public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
						if (executionCount <= retry && !(exception instanceof SSLHandshakeException)) {
							return true;
						}
						return false;
					}
				}).build();
	}

	/**
	 * 通过HTTP请求调用,获得HTTP响应内容
	 *
	 * @param requestUrl
	 *            请求URL
	 * @param isPost
	 *            是否是post方式提交
	 * @param params
	 *            参数
	 * @param requestEncoding
	 *            请求编码
	 * @param responseEncoding
	 *            响应编码
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws ClientProtocolException
	 */
	public String getHttpResponseContent(String requestUrl, Map<String, Object> params, boolean isPost,
			String requestEncoding, String responseEncoding) {
		setRequestEncoding(requestEncoding);
		setResponseEncoding(responseEncoding);
		return getHttpResponseContent(requestUrl, params, isPost);

	}

	public String getURLResponseContent(String requestUrl, Map<String, Object> params, boolean isPost,
			String requestEncoding, String responseEncoding) {
		return getHttpResponseContent(requestUrl, params, isPost, requestEncoding, responseEncoding);
	}

	/**
	 * 使用 URLConnection 连接，不需要 init()
	 * 
	 * @param requestUrl
	 * @param params
	 * @param isPost
	 * @param requestEncoding
	 * @param responseEncoding
	 * @return
	 */
	public String getURLResponseContent2(String requestUrl, Map<String, Object> params, boolean isPost,
			String requestEncoding, String responseEncoding) {
		String param = "";
		if (params != null && !params.isEmpty()) {
			List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
			for (Entry<String, Object> entry : params.entrySet()) {
				/**
				 * if (entry.getValue() == null) { continue; }
				 */
				nameValuePairList
						.add(new BasicNameValuePair(entry.getKey(), ObjectUtil.nullStrToEmpty(entry.getValue())));
			}
			param = URLEncodedUtils.format(nameValuePairList, requestEncoding);
		}
		URLRequest urlRequest = new URLRequest();
		if (isPost) {
			return urlRequest.sendPost(requestUrl, param);
		} else {
			return urlRequest.sendGet(requestUrl, param);
		}
	}

	public String getHttpResponseContent(String requestUrl, Map<String, Object> params, boolean isPost) {
		if (isPost) {
			return post(requestUrl, params);
		} else {
			return get(requestUrl, params);
		}

	}

	public String getJsonResponseContent(String url, String jsonParam) {
		HttpPost httppost = new HttpPost(url);
		StringEntity entity = new StringEntity(jsonParam.toString(), Charset.forName(requestEncoding));
		entity.setContentType("application/json");
		httppost.setEntity(entity);
		return execute(httppost, "POST");
	}

	private String get(String url, Map<String, Object> params) {

		try {

			if (params != null && !params.isEmpty()) {
				List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
				for (Entry<String, Object> entry : params.entrySet()) {

					nameValuePairList
							.add(new BasicNameValuePair(entry.getKey(), ObjectUtil.nullStrToEmpty(entry.getValue())));
				}
				String str = EntityUtils
						.toString(new UrlEncodedFormEntity(nameValuePairList, Charset.forName(requestEncoding)));
				url = url.endsWith("?") ? (url + str) : (url + "?" + str);
				HttpGet httpget = new HttpGet(url);
				return execute(httpget, "GET");
			} else {
				HttpGet httpget = new HttpGet(url);
				return execute(httpget, "GET");
			}
		} catch (ParseException e) {
			logger.error("[send-exception]" + url, e);
		} catch (Exception e) {
			logger.error("[send-exception]" + url, e);
		}
		return null;
	}

	private String post(String url, Map<String, Object> params) {
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
		HttpPost httppost = new HttpPost(url);
		boolean isFile = false;

		if (params != null && !params.isEmpty()) {
			for (Entry<String, Object> entry : params.entrySet()) {
				if (entry.getValue() instanceof File) {

					// 设置为浏览器兼容模式
					multipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
					// 设置请求的编码格式
					multipartEntityBuilder.setCharset(Charset.forName(requestEncoding));
					File file = new File(ObjectUtil.nullStrToEmpty(entry.getValue()));
					multipartEntityBuilder.addBinaryBody(entry.getKey(), file);
					isFile = true;

				} else {
					nvps.add(new BasicNameValuePair(entry.getKey(), ObjectUtil.nullStrToEmpty(entry.getValue())));

				}
			}
		}
		if (isFile) {
			// 生成 HTTP 实体
			HttpEntity httpEntity = multipartEntityBuilder.build();
			// 设置 POST 请求的实体部分
			httppost.setEntity(httpEntity);
			// 发送 HTTP 请求
			return execute(httppost, "POST");
		} else {
			httppost.setEntity(new UrlEncodedFormEntity(nvps, Charset.forName(requestEncoding)));
			return execute(httppost, "POST");
		}
	}

	private String execute(HttpRequestBase httpRequest, String method) {
		httpRequest.addHeader("User-Agent", USER_AGENT);// 防止屏蔽
		if (head != null) {
			for (Map.Entry<String, String> entry : head.entrySet()) {
				httpRequest.addHeader(entry.getKey(), entry.getValue());
			}
		}
		String respone = null;
		try {
			CloseableHttpResponse httpresponse = httpClient.execute(httpRequest);

			int statusCode = httpresponse.getStatusLine().getStatusCode();
			setHttpCode(statusCode);
			if (statusCode == HttpStatus.SC_OK) {
				HttpEntity entity = httpresponse.getEntity();
				respone = EntityUtils.toString(entity, Charset.forName(responseEncoding));
				EntityUtils.consume(entity);
				httpresponse.close();
				if (logger.isInfoEnabled()) {
					logger.info("[http-success]" + httpRequest.getURI());
				}

			} else {
				httpresponse.close();
				if (StringUtils.equals("POST", method)) {
					HttpPost http = (HttpPost) httpRequest;
					logger.error(
							StringUtil.addString(":", "[http-error-code]" + statusCode, "[uri]",
									httpRequest.getURI().toString()),
							StringUtil.inputStreamTOString(http.getEntity().getContent(), "UTF-8"));
				} else {
					logger.error(StringUtil.addString(":", "[http-error-code]" + statusCode, "[uri]",
							httpRequest.getURI().toString()));
				}
			}
		} catch (HttpHostConnectException e) {
			setHttpCode(500);
			if (StringUtils.equals("POST", method)) {
				HttpPost http = (HttpPost) httpRequest;
				try {
					logger.error(StringUtil.addString(":", "[http-exception-connect]", httpRequest.getURI().toString(),
							StringUtil.inputStreamTOString(http.getEntity().getContent(), "UTF-8")), e);
				} catch (IOException e1) {
					logger.error("[http-InputStreamTOString]", e1);
				}
			} else {
				logger.error("[http-exception-connect]" + httpRequest.getURI(), e);
			}
		} catch (Exception e) {
			setHttpCode(500);
			if (StringUtils.equals("POST", method)) {
				HttpPost http = (HttpPost) httpRequest;
				try {
					logger.error(StringUtil.addString(":", "[http-exception]", httpRequest.getURI().toString(),
							StringUtil.inputStreamTOString(http.getEntity().getContent(), "UTF-8")), e);
				} catch (IOException e1) {
					logger.error("[http-InputStreamTOString]", e1);
				}
			} else {
				logger.error("[http-exception]" + httpRequest.getURI(), e);
			}
		} finally {

			httpRequest.abort();// 释放连接
			try {
				httpClient.close();
			} catch (IOException e) {

				logger.error("[http-exception]" + httpRequest.getURI(), e);

			}

		}
		return respone;

	}

	public int getMaxConnections() {
		return maxConnections;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public int getMaxPerRoute() {
		return maxPerRoute;
	}

	public void setMaxPerRoute(int maxPerRoute) {
		this.maxPerRoute = maxPerRoute;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	public int getRetry() {
		return retry;
	}

	public void setRetry(int retry) {
		this.retry = retry;
	}

	public String getRequestEncoding() {
		return requestEncoding;
	}

	public void setRequestEncoding(String requestEncoding) {
		this.requestEncoding = requestEncoding;
	}

	public String getResponseEncoding() {
		return responseEncoding;
	}

	public void setResponseEncoding(String responseEncoding) {
		this.responseEncoding = responseEncoding;
	}

	public CloseableHttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(CloseableHttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public int getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}

	public Map<String, String> getHead() {
		return head;
	}

	public void setHead(Map<String, String> head) {
		this.head = head;
	}

	public void addHead(String key, String value) {
		if (this.head == null) {
			this.head = new HashMap<String, String>();
		}
		this.head.put(key, value);
	}

	class URLRequest {

		public String sendGet(String url, String param) {
			String result = "";
			BufferedReader in = null;
			try {
				String urlNameString = url.endsWith("?") ? (url + param) : (url + "?" + param);
				URL realUrl = new URL(urlNameString);
				// 打开和URL之间的连接
				URLConnection connection = realUrl.openConnection();
				// 设置通用的请求属性
				connection.setConnectTimeout(connectionTimeout);
				connection.setReadTimeout(readTimeout);
				connection.setRequestProperty("accept", "*/*");
				connection.setRequestProperty("connection", "Keep-Alive");
				connection.setRequestProperty("user-agent", USER_AGENT);
				connection.setRequestProperty("Accept-Charset", requestEncoding);
				connection.setRequestProperty("contentType", requestEncoding);
				// 建立实际的连接
				connection.connect();
				// 获取所有响应头字段
				// Map<String, List<String>> map = connection.getHeaderFields();
				// 遍历所有的响应头字段
				// for (String key : map.keySet()) {
				// System.out.println(key + "--->" + map.get(key));
				// }
				// 定义 BufferedReader输入流来读取URL的响应
				in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line;
				while ((line = in.readLine()) != null) {
					result += line;
				}
			} catch (Exception e) {
				logger.error("[http-exception]", e);
			}
			// 使用finally块来关闭输入流
			finally {
				try {
					if (in != null) {
						in.close();
					}
				} catch (Exception e2) {
					logger.error("[http-exception]", e2);
				}
			}
			return result;
		}

		public String sendPost(String url, String param) {
			PrintWriter out = null;
			BufferedReader in = null;
			String result = "";
			try {
				URL realUrl = new URL(url);
				// 打开和URL之间的连接
				URLConnection conn = realUrl.openConnection();
				// 设置通用的请求属性
				conn.setConnectTimeout(connectionTimeout);
				conn.setReadTimeout(readTimeout);
				conn.setRequestProperty("accept", "*/*");
				conn.setRequestProperty("connection", "Keep-Alive");
				conn.setRequestProperty("user-agent", USER_AGENT);
				conn.setRequestProperty("Accept-Charset", requestEncoding);
				conn.setRequestProperty("contentType", requestEncoding);
				// 发送POST请求必须设置如下两行
				conn.setDoOutput(true);
				conn.setDoInput(true);
				// 获取URLConnection对象对应的输出流
				out = new PrintWriter(conn.getOutputStream());
				// 发送请求参数
				out.print(param);
				// flush输出流的缓冲
				out.flush();
				// 定义BufferedReader输入流来读取URL的响应
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line;
				while ((line = in.readLine()) != null) {
					result += line;
				}
			} catch (Exception e) {
				logger.error("[http-exception]", e);
			}
			// 使用finally块来关闭输出流、输入流
			finally {
				try {
					if (out != null) {
						out.close();
					}
					if (in != null) {
						in.close();
					}
				} catch (IOException ex) {
					logger.error("[http-exception]", ex);
				}
			}
			return result;
		}
	}

	public String getUrlRequest(String url, String param) {
		URLRequest urlRequest = new URLRequest();

		return urlRequest.sendPost(url, param);
	}

	public Result<JSONObject> getResponse(String requestUrl, Map<String, Object> params, Boolean isPost,
			Integer timeout, String characterSet) {

		if (StringUtil.isEmpty(requestUrl) || isPost == null || timeout == null || StringUtil.isEmpty(characterSet)) {
			return new Result<JSONObject>(new MsgException("请求参数不完整"));
		}

		try {
			HttpClientService httpClientService = new HttpClientService();
			httpClientService.setRetry(0);
			httpClientService.setReadTimeout(timeout);
			httpClientService.init();
			// true:post请求 false:get请求
			String response = httpClientService.getHttpResponseContent(requestUrl, params, isPost, characterSet,
					characterSet);

			// 检查Http是否响应
			if (StringUtil.isEmpty(response)) {
				return new Result<JSONObject>(new MsgException("响应参数为空"));
			}

			// 格式化Http响应结果
			JSONObject json = JSONObject.fromObject(response);
			if (json == null) {
				return new Result<JSONObject>(new MsgException("响应信息格式化错误"));
			}

			return new Result<JSONObject>(json);
		} catch (Throwable e) {
			return new Result<JSONObject>(new MsgException("Http请求发生错误"));
		}
	}

	public static void main(String[] args) {
		// String url =
		// "http://api.1-tree.com.cn/memberAction/newMember.jd?papercode=542526198501021266&birthday=1985-01-02&sex=%E5%A5%B3&token=6d0a5043-4223-4cf8-9986-424a5bfc2ccc&cardnum=&cellphone=13900001119&name=%E6%B5%8B%E8%AF%95%E5%90%8D";
		// String url = "http://www.google.com";

		String url = "http://restapi.amap.com/v3/geocode/geo";
		HttpClientService httpClientService = new HttpClientService();
		httpClientService.init();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("key", "30706458f605b7f15fa7d11e4354c13b");
		params.put("s", "rsv3");
		params.put("address", "陕西省西安市雁塔区徐家庄村");
		String result = null;
		try {
			result = httpClientService.getURLResponseContent(url, params, false, "utf-8", "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(result);

	}

}
