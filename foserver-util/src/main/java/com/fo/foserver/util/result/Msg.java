package com.fo.foserver.util.result;
/**
 * 项目异常定义
 */
public enum Msg {
	
	/**
	 * 系统异常
	 */
	SUCCESS("0", "调用成功"),
	SYSTEM_ERROR("1", "系统异常"),  
	BUSINESS_ERROR("2", "业务异常"),
	LOGIN_SESSION("3", "登录"),
	LOGOUT_SESSION("4", "登出"),
	LONGIN_FAIL("5", "重新登陆"),
	PAY_SUCCESS("7", "支付成功"),
	
	;
	
	Msg(String msgCode, String msgInfo) {
		this.msgCode = msgCode;
		this.msgInfo = msgInfo;
	}
	
	private String msgCode;
	//消息详情
	private String msgInfo;
	
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgInfo() {
		return msgInfo;
	}
	public void setMsgInfo(String msgInfo) {
		this.msgInfo = msgInfo;
	}

	

}
