package com.fo.foserver.util.sms;

/**
 * 短信模板，如果后续供应商增加可以考虑使用数据库维护
 */
public enum SmsEnum {
	
	SMS_VERIFY("SMS_102215019", "佛家三宝", "验证码${code}，请于15分钟内完成验证。若非本人操作，请忽略本信息。"),
	;
	
	SmsEnum(String templateId, String sign, String template) {
		this.templateId = templateId;
		this.template = template;
		this.sign = sign;
	}

	private String templateId;

	private String template;

	private String sign;
	

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * @return the sign
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * @param sign the sign to set
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}




}
