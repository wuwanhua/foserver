package com.fo.foserver.util.base;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fo.foserver.util.serialization.Serializations;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class CacheService {
	private static final Logger logger = LoggerFactory.getLogger(CacheService.class);
	 public static final String CHARSET_NAME = redis.clients.jedis.Protocol.CHARSET;
	
	private JedisPool pool;
	private int maxIdle;
	private int maxTotal;
	private String host;
	private String password;
	private int port;
	private int timeout;
	

	public void init() {
		JedisPoolConfig config = new JedisPoolConfig();
	    //最大空闲连接数,
	    config.setMaxIdle(maxIdle);
	    //最大连接数, 
	    config.setMaxTotal(maxTotal);
	    config.setTestOnBorrow(false);
	    config.setTestOnReturn(false);
	    if (StringUtil.isEmpty(password)) {
	    	password = null;
	    }

	    try {
			this.pool = new JedisPool(config, host, port, timeout, password);
		} catch (Exception e) {
			logger.error("[redis-Exception-connect]" ,e);
		}

	}
	
	 public boolean set(String key, Object value, int expireSeconds){
	        if (null == key || null == value || pool == null) {
	        	logger.error("[redis-Exception]:pool null" + key);
	           return false;
	        }
	        Jedis jedis = null;
	        try {
	        	 byte[] valueBytes = object2Bytes(value);
	            jedis = pool.getResource();
	            jedis.set(key.getBytes(CHARSET_NAME), valueBytes);
	            if (expireSeconds != -1) {
	                jedis.expire(key.getBytes(CHARSET_NAME), expireSeconds);
	            }
	            
	        } catch (Exception e) {
	            logger.error("[redis-Exception]", e);
	            return false;
	        } finally {
	            if (jedis != null) {
	                jedis.close();
	            }
	        }
	        return true;
	    }

	    public Object get(String key){
	        if (null == key || pool == null) {
	        	logger.error("[redis-Exception]:pool null" + pool);
	            return null;
	        }
	        Jedis jedis = null;
	        byte[] valueBytes = null;
	        try {
	            jedis = pool.getResource();
	            valueBytes = jedis.get(key.getBytes(CHARSET_NAME));
	            return bytes2Object(valueBytes);
	        } catch (Exception e) {
	            logger.error("[redis-Exception]",  e);
	            return null;
	        } finally {
	            if (jedis != null) {
	                jedis.close();
	            }
	        }
	    }
	    
	    
	    public boolean delete(String key) {
	        if (null == key || pool == null) {
	        	logger.error("[redis-Exception]:pool null" + pool);
	            return false;
	        }
	        Jedis jedis = null;
	        try {
	            jedis = pool.getResource();
	            jedis.del(key.getBytes(CHARSET_NAME));
	            return true;
	        } catch (Exception e) {
	            logger.error("[redis-Exception]",  e);
	            return false;
	        } finally {
	            if (jedis != null) {
	                jedis.close();
	            }
	        }
	    }
	    
	    
	    private byte[] object2Bytes(Object value) {
	        return (byte[]) Serializations.newJavaSerialization().serialize(value);
	    }

	    private Object bytes2Object(byte[] bytes) {
	        return Serializations.newJavaSerialization().deserialize(bytes);
	    }

		public JedisPool getPool() {
			return pool;
		}

		public int getMaxIdle() {
			return maxIdle;
		}

		public int getMaxTotal() {
			return maxTotal;
		}

		public String getHost() {
			return host;
		}

		public String getPassword() {
			return password;
		}

		public int getPort() {
			return port;
		}

		public int getTimeout() {
			return timeout;
		}

		public void setPool(JedisPool pool) {
			this.pool = pool;
		}

		public void setMaxIdle(int maxIdle) {
			this.maxIdle = maxIdle;
		}

		public void setMaxTotal(int maxTotal) {
			this.maxTotal = maxTotal;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public void setPort(int port) {
			this.port = port;
		}

		public void setTimeout(int timeout) {
			this.timeout = timeout;
		}
	    
	    
	
}
