package com.fo.foserver.util.base;

public class ObjectUtil {
	
	/**
	 * 判断两个对象是否相等
	 * 
	 * @param firstObject
	 *            对象一
	 * 
	 * @param secondObject
	 *            对象二
	 * @return true or false
	 */
	public static boolean isEquals(Object firstObject, Object secondObject) {
		return firstObject == secondObject
				|| (firstObject == null ? secondObject == null : firstObject
						.equals(secondObject));
	}

	/**
	 * 判断对象是否为空
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj) {
		return obj == null ? true : obj instanceof String ? StringUtil
				.isEmpty(obj.toString()) : false;
	}

	/**
	 * 对象转字符串
	 * 
	 * @see 如果是空返回""，如果是字符串型返回字符串，其他toString
	 * @param obj
	 *            对象
	 * @return 字符串
	 */
	public static String nullStrToEmpty(Object obj) {
		return (obj == null ? "" : (obj instanceof String ? (String) obj : obj
				.toString()));
	}

}
