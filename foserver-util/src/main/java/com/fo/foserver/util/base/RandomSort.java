package com.fo.foserver.util.base;

import java.util.Random;


public class RandomSort {
	
	private Random random = new Random();    
    //数组大小    
    private int SIZE = 10;    
    //要重排序的数组    
    private static char[] positions = null;    
    private String str;
        
    public RandomSort(String str) {   
    	this.str = str;
    	this.SIZE = str.length();
    	this.positions = new char[str.length()];
    	this.positions = str.toCharArray();
    }    
        
    //重排序    
    public void changePosition() {    
        for(int index=SIZE-1; index>=0; index--) {    
            //从0到index处之间随机取一个值，跟index处的元素交换    
            exchange(random.nextInt(index+1), index);    
        }    
        printPositions();    
    }    
        
    //交换位置    
    private void exchange(int p1, int p2) {    
        char temp = positions[p1];    
        positions[p1] = positions[p2];    
        positions[p2] = temp;  //更好位置  
    }    
        
    //打印数组的值    
    private void printPositions() {    
        for(int index=0; index<SIZE; index++) {    
            System.out.print(positions[index]+" ");             
        }    
        System.out.println();    
    }    
    
    public static void main(String[] args) {    
        RandomSort rs = new RandomSort("as1234");    
        rs.changePosition();    
        rs.changePosition();    
        rs.changePosition();    
    } 
    
    public static String getCode(String code){
    	RandomSort rs = new RandomSort(code);    
        rs.changePosition();    
        rs.changePosition();    
        rs.changePosition();    
        return String.valueOf(positions);
    }

}
