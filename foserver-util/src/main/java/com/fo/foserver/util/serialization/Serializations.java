package com.fo.foserver.util.serialization;

import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * tesla Serialization type implement
 *
 * @author wuqi
 */
public class Serializations {

    /**
     * normal serialization
     *
     * @return
     */
    public static Serialization<byte[]> newJavaSerialization() {
        return new KryoSerialization();
    }

    /**
     * php JSON serialization
     *
     * @return
     */
    public static Serialization<byte[]> newPhpSerialization() {
        return new JsonSerialization(SerializerFeature.DisableCircularReferenceDetect,
                SerializerFeature.WriteNonStringKeyAsString);
    }

    public static Serialization<byte[]> newCompatibleSerialization() {
        return new JsonSerialization(SerializerFeature.WriteClassName, SerializerFeature.SkipTransientField,
                SerializerFeature.DisableCircularReferenceDetect, SerializerFeature.WriteNonStringKeyAsString);
    }
}
